/**
 * Created by Greshilov on 23.05.2017.
 */

$(document).ready(function() {
    $.ajax({
        type: 'GET',
        url: '/api/groups',
        success: function (resposne) {
            groups = resposne.map(function (item) {
                return {
                    id: item.id,
                    text: item.name,
                }
            });
            $("#group_id").select2({
                data: groups,
            });
        }
    });
});