/**
 * Created by Greshilov on 23.05.2017.
 */

$(document).ready(function() {
    $.ajax({
        type: 'GET',
        url: '/api/specialties',
        success: function (resposne) {
            specialties = resposne.map(function (item) {
                return {
                    id: item.id,
                    text: item.name,
                }
            });
            $("#specialty_id").select2({
                data: specialties,
                placeholder: 'Выберите специальность',
            });
        }
    });
});

function showGroupForm(group) {
    if(!group) {
        $('#group_id').val('');
        $('#name').val('');
        $('#specialty_id').val([]).trigger('change');
    } else {
        $('#group_id').val(group.id);
        $('#name').val(group.name);
        $('#specialty_id').val(group.specialty_id).trigger('change');
    }
    $('#name-error').empty().hide();
    $('#specialty_id-error').empty().hide();
    $('#group-form-modal').modal('show');
}

function saveGroup() {
    if ($('#group_id').val() != '') {
        saveEditedGroup();
    } else {
        saveCreatedGroup();
    }
}

function saveCreatedGroup() {
    $.ajax({
        headers: {"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr("content")},
        type: 'post',
        url: '/admin/groups/create',
        cache: false,
        contentType: false,
        processData: false,
        data: new FormData($('#group-form')[0]),
        dataType: 'json',
        success: function (res) {
            $('#group-form-modal').modal('hide');
            var group = res;
            var tr = $('<tr>', {
                class: 'group-' + group.id,
                append: $('<td>',{
                    class: 'name',
                    text: group.name,
                }).add($('<td>',{ class: 'specialty', text: group.specialty.name}))
                    .add($('<td>',{
                        append: $("<span><a href='#' onclick='showEditGroupForm(" + group.id + ")'>Редактировать</a></span>")
                            .add("<span> | </span>")
                            .add("<span><a href='#' onclick='deleteGroup(" + group.id + ")'>Удалить</a></span>")}))});
            $('table tbody tr td[colspan]').remove();
            $('table tbody').prepend(tr);
            tr.animate({backgroundColor: '#5481c5'}, 500).animate({backgroundColor: '#fff'}, 300);
        },
        error: function (res) {
            err = JSON.parse(res.responseText);
            if (err.name) {
                $('#name-error').html(err.name[0]).show();
            }
            if (err.specialty_id) {
                $('#specialty_id-error').html(err.specialty_id[0]).show();
            }
        }
    });
}

function saveEditedGroup() {
    $.ajax({
        headers: {"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr("content")},
        type: 'post',
        url: '/admin/groups/edit',
        cache: false,
        contentType: false,
        processData: false,
        data: new FormData($('#group-form')[0]),
        dataType: 'json',
        success: function (res) {
            $('#group-form-modal').modal('hide');
            var group = res;
            var tr = $('.group-' + group.id);
            $(tr).find('td.name').html(group.name);
            $(tr).find('td.specialty').html(group.specialty.name);
            $('table tbody tr td[colspan]').remove();
            $('table tbody').prepend(tr);
            tr.animate({backgroundColor: '#5481c5'}, 500).animate({backgroundColor: '#fff'}, 300);
        },
        error: function (res) {
            err = JSON.parse(res.responseText);
            if (err.name) {
                $('#name-error').html(err.name[0]).show();
            }
            if (err.specialty_id) {
                $('#specialty_id-error').html(err.info[0]).show();
            }
        }
    });


}

function deleteGroup(group_id) {
    if (confirm('Вы действительно хотите удалить эту группу?')) {
        $.ajax({
            type: 'GET',
            url: '/admin/groups/delete',
            data: {
                group_id: group_id,
            },
            success: function (res) {
                var tr = $('.group-' + group_id);
                tr.fadeToggle(400, function () {
                    tr.remove();
                    if ($('tbody tr').length == 0) {
                        $('tbody').append($("<tr><td colspan='3'>Нет данных</td></tr>"))
                    }
                });
            },
            error: function (res) {
                console.error(res);
            }
        });
    }
}

function showEditGroupForm(group_id) {
    $.ajax({
        type: 'GET',
        url: '/admin/group',
        data: {
            group_id: group_id,
        },
        success: function (res) {
            var group = res;
            showGroupForm(group);
        },
        error: function (res) {
            console.error(res);
        }
    });
}