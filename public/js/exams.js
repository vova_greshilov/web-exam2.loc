/**
 * Created by Greshilov on 31.05.2017.
 */

if(window.location.pathname == '/teacher/exam') {
    $().ready(function () {
        $('.filter_specialty').select2();
        $('.filter_discipline').select2();
        $('.filter_subject').select2();
    });

    function loadDisciplines(sender) {
        $.ajax({
            type: 'get',
            url: '/api/disciplines',
            data: {
                specialty_id: $(sender).val(),
            },
            success: function (res) {
                var selectData = [{id:'all', text: 'Все'}];
                selectData = selectData.concat(res.map(function (item) {
                    return {
                        id: item.id,
                        text: item.name,
                    }
                }));
                $('.filter_discipline').select2().empty();
                $('.filter_discipline').select2({
                    data: selectData,
                });
            },
            error: function (res) {
                console.error(res);
            }
        });
    }

    function loadSubjects(sender) {
        $.ajax({
            type: 'get',
            url: '/api/subjects',
            data: {
                discipline_id: $(sender).val(),
            },
            success: function (res) {
                var selectData = [{id:'all', text: 'Все'}];
                selectData = selectData.concat(res.map(function (item) {
                    return {
                        id: item.id,
                        text: item.name,
                    }
                }));
                $('.filter_subject').select2().empty();
                $('.filter_subject').select2({
                    data: selectData,
                });
            },
            error: function (res) {
                console.error(res);
            }
        });
    }

    function changeSubject(sender) {
        $('#subject_id').select2({
            data: $(sender).select2('data'),
        });
        $('#subject_id').val($(sender).val()).trigger('change');
    }

    function showExamForm() {
        $('#questionCount').val('');
        $('#open_questions_count').val('');
        $('#mapping_questions_count').val('');
        $('#sequence_questions_count').val('');
        $('#exam-form-modal').modal('show');
    }

    function saveExam() {
        $('#subject_id-error').hide();
        $('#questionCount-error').hide();
        $('#open_questions_count-error').hide();
        $('#mapping_questions_count-error').hide();
        $('#sequence_questions_count-error').hide();
        $.ajax({
            headers: {"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr("content")},
            type: 'post',
            url: '/teacher/exam/create',
            cache: false,
            contentType: false,
            processData: false,
            data: new FormData($('#exam-form')[0]),
            dataType: 'json',
            success: function (res) {
                $('#exam-form-modal').modal('hide');
                var exam = res;
                var tr = $("<tr class='exam-" + exam.id + " success'>" +
                    "           <td>" + exam.subject.name + "</td>" +
                    "           <td>" + exam.subject.discipline.name + "</td>" +
                    "           <td>" + exam.subject.discipline.specialty.name + "</td>" +
                    "           <td>" +
                    "               <span><a class='toggle' href='#' onclick='editExam(" + exam.id + ")'>Деактивировать</a></span>" +
                    "               <span> | </span><span><a href='#' onclick='deleteExam(" + exam.id + ")'>Удалить</a></span>" +
                    "           </td>" +
                    "       </tr>");
                $('table tbody tr td[colspan]').remove();
                $('table tbody').prepend(tr);
                tr.animate({backgroundColor: '#5481c5'}, 500).animate({backgroundColor: '#fff'}, 300);
            },
            error: function (res) {
                err = JSON.parse(res.responseText);
                if (err.subject_id) {
                    $('#subject_id-error').html("Укажите тему").show();
                }
                if (err.questionCount) {
                    $('#questionCount-error').html("Укажите количество вопросов закрытого типа (минимум 2)").show();
                }
                if (err.open_questions_count) {
                    $('#open_questions_count-error').html("Укажите количество вопросов открытого типа (минимум 2)").show();
                }
                if (err.mapping_questions_count) {
                    $('#mapping_questions_count-error').html("Укажите количество вопросов на сопоставление(минимум 2)").show();
                }
                if (err.sequence_questions_count) {
                    $('#sequence_questions_count-error').html("Укажите количество вопросов на составление последовательностей (минимум 2)").show();
                }
            }
        });
    }

    function deleteExam(exam_id) {
        if (confirm('Вы действительно хотите удалить этот тест?')) {
            $.ajax({
                type: 'GET',
                url: '/teacher/exam/delete',
                data: {
                    exam_id: exam_id,
                },
                success: function (res) {
                    var tr = $('.exam-' + exam_id);
                    tr.fadeToggle(400, function () {
                        tr.remove();
                        if ($('tbody tr').length == 0) {
                            $('tbody').append($("<tr><td colspan='4'>Нет данных</td></tr>"))
                        }
                    });
                },
                error: function (res) {
                    console.error(res);
                }
            });
        }
    }

    function editExam(exam_id) {
        $.ajax({
            type: 'GET',
            url: '/teacher/exam/edit',
            data: {
                exam_id: exam_id,
            },
            success: function (res) {
                var exam = res;
                var tr = $('.exam-' + exam_id);
                if (exam.is_active) {
                    tr.removeClass('active').addClass('success');
                    tr.find('a.toggle').html('Деактивировать');
                } else {
                    tr.removeClass('success').addClass('active');
                    tr.find('a.toggle').html('Активировать');
                }
            },
            error: function (res) {
                console.error(res);
            }
        });
    }
}