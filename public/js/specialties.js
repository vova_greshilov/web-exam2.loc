/**
 * Created by Greshilov on 24.05.2017.
 */

function showSpecForm(spec) {
    if(!spec) {
        $('#spec_id').val('');
        $('#name').val('');
    } else {
        $('#spec_id').val(spec.id);
        $('#name').val(spec.name);
    }
    $('#name-error').empty().hide();
    $('#specialty_id-error').empty().hide();
    $('#spec-form-modal').modal('show');
}

function saveSpec() {
    if ($('#spec_id').val() != '') {
        saveEditedSpec();
    } else {
        saveCreatedSpec();
    }
}

function saveCreatedSpec() {
    $.ajax({
        headers: {"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr("content")},
        type: 'post',
        url: '/admin/specs/create',
        cache: false,
        contentType: false,
        processData: false,
        data: new FormData($('#spec-form')[0]),
        dataType: 'json',
        success: function (res) {
            $('#spec-form-modal').modal('hide');
            var spec = res;
            var tr = $('<tr>', {
                class: 'spec-' + spec.id,
                append: $('<td>',{
                    class: 'name',
                    text: spec.name,
                }).add($('<td>',{
                        append: $("<span><a href='#' onclick='showEditSpecForm(" + spec.id + ")'>Редактировать</a></span>")
                            .add("<span> | </span>")
                            .add("<span><a href='#' onclick='deleteSpec(" + spec.id + ")'>Удалить</a></span>")}))});
            $('table tbody tr td[colspan]').remove();
            $('table tbody').prepend(tr);
            tr.animate({backgroundColor: '#5481c5'}, 500).animate({backgroundColor: '#fff'}, 300);
        },
        error: function (res) {
            err = JSON.parse(res.responseText);
            if (err.name) {
                $('#name-error').html(err.name[0]).show();
            }
        }
    });
}

function saveEditedSpec() {
    $.ajax({
        headers: {"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr("content")},
        type: 'post',
        url: '/admin/specs/edit',
        cache: false,
        contentType: false,
        processData: false,
        data: new FormData($('#spec-form')[0]),
        dataType: 'json',
        success: function (res) {
            $('#spec-form-modal').modal('hide');
            var spec = res;
            var tr = $('.spec-' + spec.id);
            $(tr).find('td.name').html(spec.name);
            $('table tbody tr td[colspan]').remove();
            $('table tbody').prepend(tr);
            tr.animate({backgroundColor: '#5481c5'}, 500).animate({backgroundColor: '#fff'}, 300);
        },
        error: function (res) {
            err = JSON.parse(res.responseText);
            if (err.name) {
                $('#name-error').html(err.name[0]).show();
            }
        }
    });
}

function deleteSpec(spec_id) {
    if (confirm('Вы действительно хотите удалить эту специальность?')) {
        $.ajax({
            type: 'GET',
            url: '/admin/specs/delete',
            data: {
                spec_id: spec_id,
            },
            success: function (res) {
                var tr = $('.spec-' + spec_id);
                tr.fadeToggle(400, function () {
                    tr.remove();
                    if ($('tbody tr').length == 0) {
                        $('tbody').append($("<tr><td colspan='2'>Нет данных</td></tr>"))
                    }
                });
            },
            error: function (res) {
                console.error(res);
            }
        });
    }
}

function showEditSpecForm(spec_id) {
    $.ajax({
        type: 'GET',
        url: '/admin/spec',
        data: {
            spec_id: spec_id,
        },
        success: function (res) {
            var spec = res;
            showSpecForm(spec);
        },
        error: function (res) {
            console.error(res);
        }
    });
}