/**
 * Created by Greshilov on 24.05.2017.
 */

$(document).ready(function() {
    $.ajax({
        type: 'GET',
        url: '/api/specialties',
        success: function (resposne) {
            specialties = resposne.map(function (item) {
                return {
                    id: item.id,
                    text: item.name,
                }
            });
            $("#specialty_id").select2({
                data: specialties,
                placeholder: 'Выберите специальность',
            });
            $("#filter_specialty").select2({
                placeholder: 'Cпециальность',
            });
        }
    });
});

function showDisciplineForm(discipline) {
    if(!discipline) {
        $('#discipline_id').val('');
        $('#name').val('');
        $('#specialty_id').val([]).trigger('change');
    } else {
        $('#discipline_id').val(discipline.id);
        $('#name').val(discipline.name);
        $('#specialty_id').val(discipline.specialty_id).trigger('change');
    }
    $('#name-error').empty().hide();
    $('#specialty_id-error').empty().hide();
    $('#discipline-form-modal').modal('show');
}

function saveDiscipline() {
    if ($('#discipline_id').val() != '') {
        saveEditedDiscipline();
    } else {
        saveCreatedDiscipline();
    }
}

function saveCreatedDiscipline() {
    $.ajax({
        headers: {"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr("content")},
        type: 'post',
        url: '/admin/disciplines/create',
        cache: false,
        contentType: false,
        processData: false,
        data: new FormData($('#discipline-form')[0]),
        dataType: 'json',
        success: function (res) {
            $('#discipline-form-modal').modal('hide');
            var discipline = res;
            var tr = $('<tr>', {
                class: 'discipline-' + discipline.id,
                append: $('<td>',{
                    class: 'name',
                    text: discipline.name,
                }).add($('<td>',{ class: 'specialty', text: discipline.specialty.name}))
                    .add($('<td>',{
                        append: $("<span><a href='#' onclick='showEditDisciplineForm(" + discipline.id + ")'>Редактировать</a></span>")
                            .add("<span> | </span>")
                            .add("<span><a href='#' onclick='deleteDiscipline(" + discipline.id + ")'>Удалить</a></span>")}))});
            $('table tbody tr td[colspan]').remove();
            $('table tbody').prepend(tr);
            tr.animate({backgroundColor: '#5481c5'}, 500).animate({backgroundColor: '#fff'}, 300);
        },
        error: function (res) {
            err = JSON.parse(res.responseText);
            if (err.name) {
                $('#name-error').html(err.name[0]).show();
            }
            if (err.specialty_id) {
                $('#specialty_id-error').html(err.specialty_id[0]).show();
            }
        }
    });
}

function saveEditedDiscipline() {
    $.ajax({
        headers: {"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr("content")},
        type: 'post',
        url: '/admin/disciplines/edit',
        cache: false,
        contentType: false,
        processData: false,
        data: new FormData($('#discipline-form')[0]),
        dataType: 'json',
        success: function (res) {
            $('#discipline-form-modal').modal('hide');
            var discipline = res;
            var tr = $('.discipline-' + discipline.id);
            $(tr).find('td.name').html(discipline.name);
            $(tr).find('td.specialty').html(discipline.specialty.name);
            $('table tbody tr td[colspan]').remove();
            $('table tbody').prepend(tr);
            tr.animate({backgroundColor: '#5481c5'}, 500).animate({backgroundColor: '#fff'}, 300);
        },
        error: function (res) {
            err = JSON.parse(res.responseText);
            if (err.name) {
                $('#name-error').html(err.name[0]).show();
            }
            if (err.specialty_id) {
                $('#specialty_id-error').html(err.info[0]).show();
            }
        }
    });


}

function deleteDiscipline(discipline_id) {
    if (confirm('Вы действительно хотите удалить эту дисциплину?')) {
        $.ajax({
            type: 'GET',
            url: '/admin/disciplines/delete',
            data: {
                discipline_id: discipline_id,
            },
            success: function (res) {
                var tr = $('.discipline-' + discipline_id);
                tr.fadeToggle(400, function () {
                    tr.remove();
                    if ($('tbody tr').length == 0) {
                        $('tbody').append($("<tr><td colspan='3'>Нет данных</td></tr>"))
                    }
                });
            },
            error: function (res) {
                console.error(res);
            }
        });
    }
}

function showEditDisciplineForm(discipline_id) {
    $.ajax({
        type: 'GET',
        url: '/admin/discipline',
        data: {
            discipline_id: discipline_id,
        },
        success: function (res) {
            var discipline = res;
            showDisciplineForm(discipline);
        },
        error: function (res) {
            console.error(res);
        }
    });
}
