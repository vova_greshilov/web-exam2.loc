/**
 * Created by Greshilov on 24.05.2017.
 */

$().ready(function () {
    $('.filter_specialty').select2();
    $('.filter_discipline').select2({
        placeholder: 'Выберите дисциплину',
    });
})

function loadDisciplines(sender) {
    $.ajax({
        type: 'get',
        url: '/api/disciplines',
        data: {
            specialty_id: $(sender).val(),
        },
        success: function (res) {
            var selectData = [{id:'all', text: 'Все'}];
            selectData = selectData.concat(res.map(function (item) {
                return {
                    id: item.id,
                    text: item.name,
                }
            }));
            $('.filter_discipline').select2().empty();
            $('.filter_discipline').select2({
                data: selectData,
            });
        },
        error: function (res) {
            console.error(res);
        }
    });
}

function showSubjectForm(subject_id) {
    $('.import-form').hide()
    if (subject_id) {
        getSubject(subject_id);
        $('.import-form').show();
    } else {
        $('.subject_id').val('');
        $('#name').val('');
        $('#discipline_id').empty();
        $('#discipline_id').prop('disabled', false);
    }
    $('#subject-form-modal').modal('show');
}

function saveSubject() {
    $('#name-error').empty().hide();
    $('#discipline_id-error').empty().hide();
    if ($('[name=subject_id]').val() != '') {
        saveEditedSubject()
    } else {
        saveCreatedSubject();
    }
}

function saveCreatedSubject() {
    $.ajax({
        headers: {"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr("content")},
        type: 'post',
        url: '/teacher/subjects/create',
        cache: false,
        contentType: false,
        processData: false,
        data: new FormData($('#subject-form')[0]),
        dataType: 'json',
        success: function (res) {
            $('#subject-form-modal').modal('hide');
            var subject = res;
            var tr = $('<tr>', {
                class: 'subject-' + subject.id,
                append: $('<td>',{
                    class: 'name',
                    text: subject.name,
                }).add($("<td>" + subject.discipline.name + "</td>"))
                    .add($("<td>" + subject.discipline.name + "</td>"))
                    .add($('<td>',{
                    append: $("<span><a href='#' onclick='showSubjectForm(" + subject.id + ")'>Редактировать</a></span>")
                        .add("<span> | </span>")
                        .add("<span><a href='#' onclick='deleteSubject(" + subject.id + ")'>Удалить</a></span>")}))});
            $('table tbody tr td[colspan]').remove();
            $('table tbody').prepend(tr);
            tr.animate({backgroundColor: '#5481c5'}, 500).animate({backgroundColor: '#fff'}, 300);
        },
        error: function (res) {
            err = JSON.parse(res.responseText);
            if (err.text) {
                $('#name-error').html(err.text[0]).show();
            }
            if (err.discipline_id) {
                $('#discipline_id-error').html(err.discipline_id[0]).show();
            }
        }
    });
}

function saveEditedSubject() {
    $.ajax({
        headers: {"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr("content")},
        type: 'post',
        url: '/teacher/subjects/edit',
        cache: false,
        contentType: false,
        processData: false,
        data: new FormData($('#subject-form')[0]),
        dataType: 'json',
        success: function (res) {
            $('#subject-form-modal').modal('hide');
            var subject = res;
            var tr = $('.subject-' + subject.id);
            $(tr).find('td.name').html(subject.name);
            $('table tbody tr td[colspan]').remove();
            $('table tbody').prepend(tr);
            tr.animate({backgroundColor: '#5481c5'}, 500).animate({backgroundColor: '#fff'}, 300);
        },
        error: function (res) {
            err = JSON.parse(res.responseText);
            if (err.text) {
                $('#name-error').html(err.text[0]).show();
            }
        }
    });
}

function deleteSubject(subject_id) {
    if (confirm('Вы действительно хотите удалить этот вопрос?')) {
        $.ajax({
            type: 'GET',
            url: '/teacher/subjects/delete',
            data: {
                subject_id: subject_id,
            },
            success: function (res) {
                var tr = $('.subject-' + subject_id);
                tr.fadeToggle(400, function () {
                    tr.remove();
                    if ($('tbody tr').length == 0) {
                        $('tbody').append($("<tr><td colspan='2'>Нет данных</td></tr>"))
                    }
                });
            },
            error: function (res) {
                console.error(res);
            }
        });
    }
}

function getSubject(subject_id) {
    $.ajax({
        headers: {"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr("content")},
        type: 'get',
        url: '/teacher/subject',
        data: {
            subject_id: subject_id,
        },
        success: function (res) {
            var subject  = res;
            $(".subject_id").val(subject_id);
            $('#name').val(subject.name);
            $('#discipline_id').select2({
                data: [{
                        id: subject.discipline.id,
                        text: subject.discipline.name,
                }],
            });
            $("#discipline_id").prop("disabled", true);
        },
        error: function (res) {
            console.error(res);
            return null;
        }
    });
}