/**
 * Created by Greshilov on 24.05.2017.
 */

function editUserPrivelegies(user_id, sender) {
    $.ajax({
        headers: {"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr("content")},
        type: 'post',
        url: '/admin/users/edit',
        data: {
            access: $(sender).val(),
            user_id: user_id,
        },
        success: function (res) {
            tr = $('tr.user-'+user_id);
            $('table tbody').prepend(tr);
            tr.animate({backgroundColor: '#5481c5'}, 500).animate({backgroundColor: '#fff'}, 300);
        }
    });

}
