/**
 * Created by Greshilov on 31.05.2017.
 */

if (window.location.pathname == '/student/exam') {
    $('.confirmation').on('click', function () {
        return confirm('Полученные результаты будут сохранены в базе как окончательные.\n' +
            'Вы уверены, что хотите начать тест?');
    });
}
var regex = new RegExp("\\/student\\/exam\\/\\d+");
var questionCounter = 0;
if (regex.test(window.location.pathname)) {

    /**
     * Function to get json of question
     * @return array of questions
     */
    function getQuestion() {
        $.ajax({
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            type: 'GET',
            url: '/student/exam/question',
            data: {
                stat_id: $('#stat-id').val(),
            },
            success: function (res) {
                var question = res;
                $('#buttons-div').css('display','block');
                drawQuestion(question);
            },
            error: function (err) {
                if (err.status == 303) {
                    //redirect on result page
                    err = JSON.parse(err.responseText);
                    window.location.replace(err.redirectUrl);
                    //window.location.href = err.redirectUrl;
                }
                //show error
                $('html').html(err.responseText);
            }
        });
    }
    getQuestion();

    //confirm button event
    //make ajax request insteadform submit
    $('#btn-confirm').on('click', function(e) {
        e.preventDefault();
        var selectedAnswers = $("input:checked");
        var answers = [-1];
        var answer = $('#answer').val();
        for (var i=0; i<selectedAnswers.length; i++) {
            answers.push(selectedAnswers[i].value);
        }
        $.ajax({
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            type: 'POST',
            url: '/student/exam/question/confirm',
            data: {
                question_id: $('#question-id').val(),
                stat_id: $('#stat-id').val(),
                answers: answers,
                answer: answer
            },
            success: function (res) {
                if (res !== 'passed') {
                    if (res.is_right) {
                        alert('Правильно!');
                        $('#text').addClass('bg-success', {duration: 500}).removeClass('bg-success', {duration: 300});
                    } else {
                        alert('Не Правильно!\n' +
                            'Правильный ответ: ' + res.answerText);
                        $('#text').addClass('bg-danger', {duration: 500}).removeClass('bg-danger', {duration: 300});
                    }
                }
                getQuestion();
            },
            error: function (err) {
                $('html').html(err.responseText);
            }
        });
    });


    function drawQuestion(data) {
        console.log('draw question!');
        var question = data.question;
        $('.current-question').html(data.index + 1);
        $('#question-id').val(question.id);

        switch (question.type) {
            case 'open':
                $('#type').html('Ввод правильного ответа').attr('title', 'Ответ выбирается из указанных');
                break;
            case 'selectable':
                $('#type').html('Выбор правильного ответа').attr('title', 'Ответ вводится в точной форме');
                break;
            case 'sequence':
                $('#type').html('Выстраивание последовательности').attr('title', 'Ответ должен иметь примерно такой формат - 1, 2, 3, 4, 5');
                break;
            case 'mapping':
                $('#type').html('Сопоставление вариантов').attr('title', 'Ответ должен выглядеть примерно так - 1-Б, 2-Г, 3-Д, 4-А, 5-В');
                break;
        }

        $('#type').show();

        $('#text').html(question.text);
        if(!question.img)
            $('image').css('display','none');
        else
            loadImage(question.img);
        var answersDiv = $("#answers-div");
        var answerDiv = $("#answer-div");
        if(question.type == 'selectable') {
            answersDiv.show();
            answerDiv.hide();
            answersDiv.children().remove();
            question.answers.forEach(function (answer) {
                var answerDiv = $("<div class='checkbox'></div>")
                    .append($("<label><input type='checkbox' name='answers[]' value='" + answer.id + "'>" + answer.text + "</label>"));
                answersDiv.append(answerDiv);
            });
        } else {
            answersDiv.hide();
            answerDiv.show();
            $('#answer').val('');
        }

    }

    function loadImage(src){
        var img = $("#img").attr('src', src)
            .on('load', function() {
                if (!this.complete || typeof this.naturalWidth == "undefined" || this.naturalWidth == 0)
                    console.error("failed to load image '" + src + "'");
                else $('#img').css('display','block');
            });
    }
}

