/**
 * Created by Greshilov on 03.06.2017.
 */

if(window.location.pathname == '/stat') {
    $().ready(function () {
        $('.filter_specialty').select2();
        $('.filter_discipline').select2();
        $('.filter_subject').select2();
    });

    function loadDisciplines(sender) {
        $.ajax({
            type: 'get',
            url: '/api/disciplines',
            data: {
                specialty_id: $(sender).val(),
            },
            success: function (res) {
                var selectData = [{id:'all', text: 'Все'}];
                selectData = selectData.concat(res.map(function (item) {
                    return {
                        id: item.id,
                        text: item.name,
                    }
                }));
                $('.filter_discipline').select2().empty();
                $('.filter_discipline').select2({
                    data: selectData,
                });
            },
            error: function (res) {
                console.error(res);
            }
        });
    }

    function loadSubjects(sender) {
        $.ajax({
            type: 'get',
            url: '/api/subjects',
            data: {
                discipline_id: $(sender).val(),
            },
            success: function (res) {
                var selectData = [{id:'all', text: 'Все'}];
                selectData = selectData.concat(res.map(function (item) {
                    return {
                        id: item.id,
                        text: item.name,
                    }
                }));
                $('.filter_subject').select2().empty();
                $('.filter_subject').select2({
                    data: selectData,
                });
            },
            error: function (res) {
                console.error(res);
            }
        });
    }
}