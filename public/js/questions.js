/**
 * Created by Greshilov on 24.05.2017.
 */

if(window.location.pathname == '/teacher/questions') {
    $().ready(function () {
        $('.filter_specialty').select2();
        $('.filter_discipline').select2();
        $('.filter_subject').select2();
        changeSubject($('select#filter_subject'));
    });

    function loadDisciplines(sender) {
        $.ajax({
            type: 'get',
            url: '/api/disciplines',
            data: {
                specialty_id: $(sender).val(),
            },
            success: function (res) {
                var selectData = [{id:'all', text: 'Все'}];
                selectData = selectData.concat(res.map(function (item) {
                    return {
                        id: item.id,
                        text: item.name,
                    }
                }));
                $('.filter_discipline').select2().empty();
                $('.filter_discipline').select2({
                    data: selectData,
                });
            },
            error: function (res) {
                console.error(res);
            }
        });
    }

    function loadSubjects(sender) {
        $.ajax({
            type: 'get',
            url: '/api/subjects',
            data: {
                discipline_id: $(sender).val(),
            },
            success: function (res) {
                var selectData = [{id:'all', text: 'Все'}];
                selectData = selectData.concat(res.map(function (item) {
                    return {
                        id: item.id,
                        text: item.name,
                    }
                }));
                $('.filter_subject').select2().empty();
                $('.filter_subject').select2({
                    data: selectData,
                });
            },
            error: function (res) {
                console.error(res);
            }
        });
    }

    function changeSubject(sender) {
        if ($(sender).val() == 'all') {
            $('#create_btn').attr('disabled', true);
        } else {
            $('#subject_id').select2({
                data: $(sender).select2('data'),
                minimumResultsForSearch: Infinity
            });
            $('#subject_id').val($(sender).val()).trigger('change');
            $('#create_btn').removeAttr('disabled');
        }
    }

    function showQuestionForm(question_id) {
        if (question_id) {
            getQuestion(question_id);
        } else {
            $('#question_id').val('');
            $('#text').val('');
            $('#img').removeAttr('src');
        }
        $('.answers-group').empty();
        $('#question-form-modal').modal('show');
        return false;
    }

    function saveQuestion() {
        $('#text-error').empty().hide();
        $('#subject_id-error').empty().hide();
        $('#img-error').empty().hide();
        $('#answers-error').empty().hide();
        if ($('#question_id').val() != '') {
            saveEditedQuestion();
        } else {
            saveCreatedQuestion();
        }
    }

    function saveCreatedQuestion() {
        $.ajax({
            headers: {"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr("content")},
            type: 'post',
            url: '/teacher/questions/create',
            cache: false,
            contentType: false,
            processData: false,
            data: new FormData($('#question-form')[0]),
            dataType: 'json',
            success: function (res) {
                $('#question-form-modal').modal('hide');
                var question = res;
                var questionType = '';
                switch (question.type) {
                    case 'open': questionType='Ввод правильного ответа'; break;
                    case 'selectable': questionType='Выбор правильного ответа'; break;
                    case 'sequence': questionType='Выстраивание последовательности'; break;
                    case 'mapping': questionType='Сопоставление вариантов'; break;
                }
                var tr = $("<tr class='question-"+question.id+"'>" +
                    "           <td>" +
                    "               <span>" +
                    "                   <code class='label label-success'>" + questionType+ "</code>" +
                    "               </span>" +
                    "               <span class='text'>" + question.text + "</span>" +
                    "           </td>" +
                    "           <td>" +
                    "               <span><a href='#' onclick='showQuestionForm("+question.id+")'>Редакиторовать</a></span> " +
                    "               <span> | </span> <span><a href='#' onclick='deleteQuestion("+question.id+")'>Удалить</a></span>" +
                    "           </td>" +
                    "       </tr>");
                $('table tbody tr td[colspan]').remove();
                $('table tbody').prepend(tr);
                tr.animate({backgroundColor: '#5481c5'}, 500).animate({backgroundColor: '#fff'}, 300);
            },
            error: function (res) {
                err = JSON.parse(res.responseText);
                if (err.text) {
                    $('#text-error').html(err.text[0]).show();
                }
                if (err.subject_id) {
                    $('#subject_id-error').html(err.subject_id[0]).show();
                }
                if (err.img) {
                    $('#img-error').html(err.img[0]).show();
                }
                if (err['answers.text']) {
                    $('#answers-error').html("Обязательно укажите текст ответа").show();
                }
                if (err.answers) {
                    $('#answers-error').html("У вопроса обязательно должен быть хотя бы один ответ").show();
                }
            }
        });
    }

    function saveEditedQuestion() {
        $.ajax({
            headers: {"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr("content")},
            type: 'post',
            url: '/teacher/questions/edit',
            cache: false,
            contentType: false,
            processData: false,
            data: new FormData($('#question-form')[0]),
            dataType: 'json',
            success: function (res) {
                $('#question-form-modal').modal('hide');
                var question = res;
                var tr = $('.question-' + question.id);
                $(tr).find('td span.text').html(question.text);
                $('table tbody').prepend(tr);
                tr.animate({backgroundColor: '#5481c5'}, 500).animate({backgroundColor: '#fff'}, 300);
            },
            error: function (res) {
                err = JSON.parse(res.responseText);
                if (err.text) {
                    $('#text-error').html(err.text[0]).show();
                }
                if (err.subject_id) {
                    $('#subject_id-error').html(err.subject_id[0]).show();
                }
                if (err.img) {
                    $('#img-error').html(err.img[0]).show();
                }
                if (err['answers.text']) {
                    $('#answers-error').html("Обязательно укажите текст ответа").show();
                }
                if (err.answers) {
                    $('#answers-error').html("У вопроса обязательно должен быть хотя бы один ответ").show();
                }
            }
        });
    }

    function deleteQuestion(question_id) {
        if (confirm('Вы действительно хотите удалить этот вопрос?')) {
            $.ajax({
                type: 'GET',
                url: '/teacher/questions/delete',
                data: {
                    question_id: question_id,
                },
                success: function (res) {
                    var tr = $('.question-' + question_id);
                    tr.fadeToggle(400, function () {
                        tr.remove();
                        if ($('tbody tr').length == 0) {
                            $('tbody').append($("<tr><td colspan='2'>Нет данных</td></tr>"))
                        }
                    });
                },
                error: function (res) {
                    console.error(res);
                }
            });
        }
    }

    function getQuestion(question_id) {
        $.ajax({
            headers: {"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr("content")},
            type: 'get',
            url: '/teacher/question',
            data: {
                question_id: question_id,
            },
            success: function (res) {
                var question = res;
                $('#question_id').val(question_id);
                $('#subject_id').select2({
                    data: [{
                        id: question.subject.id,
                        text: question.subject.name,
                    }],
                    minimumResultsForSearch: Infinity
                });
                $('#subject_id').val(question.subject_id).trigger('change');
                $('#type').val(question.type);
                $('#text').val(question.text);
                loadImage(question.img)
                // $('#img').attr('src', );
                question.answers.forEach(function (answer) {
                    addAnswer(answer)
                });
            },
            error: function (res) {
                console.error(res);
                return null;
            }
        });
    }

    function addAnswer(answer) {
        var $answersGroup = $('.answers-group');
        var questionCounter = $answersGroup.find('.answer').length;
        var $answer = $("<div class='row form-group answer answer-" + (answer ? answer.id : "") + "'>" +
            "   <input type='hidden' name='answers[" + questionCounter + "][id]' value='"+(answer ? answer.id: "")+"'>" +
            "   <div class='col-xs-9'>" +
            "       <textarea name='answers[" + questionCounter + "][text]' value='' class='form-control'>"+(answer ? answer.text: "")+"</textarea>" +
            "   </div>" +
            "   <div class='col-xs-3'>" +
            "      <div class='form-check form-check-inline'>" +
            "           <label class='form-check-label'>" +
            "           <input name='answers[" + questionCounter + "][is_right]' type='checkbox' " +
            "               value='checked' "+(answer ? answer.is_right ? "checked": "" : "")+" class='form-check-input'>" +
            "                   Правильный ответ </label>" +
            "           <br/><a href='#' onclick='deleteAnswer(" + ((answer!=undefined) ? answer.id : "-1") + ")'>Удалить ответ</a>" +
            "     </div>" +
            "   </div>" +
            "</div>");
        $answer.find("[name='answer[0][id]']").val('');
        $answer.find("[name='answer[0][text]']").val('');
        $answer.find("[name='answer[0][is_right]']").prop('checked', false); // Unchecks it
        $('.answers-group').append($answer);
    }

    function deleteAnswer(answer_id) {
        if (answer_id==-1) {
            var tr = $('.answer-');
            tr.fadeToggle(400, function () {
                tr.remove();
            });
            return;
        }
        if (confirm('Вы действительно хотите удалить этот ответ?')) {
            $.ajax({
                type: 'GET',
                url: '/teacher/questions/answer/detach',
                data: {
                    answer_id: answer_id,
                },
                success: function (res) {
                    var answer_id = res;
                    var tr = $('.answer-' + answer_id);
                    tr.fadeToggle(400, function () {
                        tr.remove();
                    });
                },
                error: function (res) {
                    console.error(res);
                }
            });
        }
    }

    function loadImage(src){
        var img = $(".img-preview").attr('src', src)
            .on('load', function() {
                if (!this.complete || typeof this.naturalWidth == "undefined" || this.naturalWidth == 0)
                    console.error("failed to load image '" + src + "'");
                else $('image').css('display','block');
            });
    }
}


