<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ExamQuestion
 *
 * @property-read \App\Question $question
 * @mixin \Eloquent
 */
class ExamQuestion extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'is_passed', 'question_id', 'stat_id',
    ];

    protected $table = 'exam_questions';

    public $timestamps = false;

    public function question()
    {
        return $this->belongsTo('App\Question','question_id');
    }
}
