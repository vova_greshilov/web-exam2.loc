<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Question
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Answer[] $answers
 * @property-read \App\User $autor
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Answer[] $rightAnswers
 * @property-read \App\Subject $subject
 * @mixin \Eloquent
 */
class Question extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type', 'text', 'img', 'subject_id','user_id',
    ];

    protected $table = 'questions';

    public $timestamps = false;

    public function autor()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function subject()
    {
        return $this->belongsTo('App\Subject','subject_id');
    }

    public function answers()
    {
        return $this->hasMany('App\Answer','question_id');
    }

    public function rightAnswers() {
        return $this->hasMany('App\Answer','question_id')->where('is_right',true);
    }
}