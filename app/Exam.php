<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

/**
 * App\Exam
 *
 * @property-read \App\User $creator
 * @property-read mixed $questions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Statistic[] $statistics
 * @property-read \App\Subject $subject
 * @mixin \Eloquent
 */
class Exam extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'close_count', 'open_count', 'sequence_count', 'mapping_count', 'is_active', 'subject_id', 'user_id',
    ];

    protected $appends = [
        'questions_count',
    ];

    protected $table = 'exams';

    public function subject()
    {
        return $this->belongsTo('App\Subject','subject_id');
    }

    /**
     * Autor of Exam
     *
     **/
    public function creator()
    {
        return $this->belongsTo('App\User','user_id');
    }

    /**
     * Questions array for testing students
     *
     * @return random array of questions size of 'QuestionCount'
     *
     **/
    public function questions()
    {
        $outputQuestions = collect([]);
        $allQuestions = $this->subject->close_questions->load('answers')->shuffle();
        if ($allQuestions->count() != 0) {
            for( $i = 0; $i < $this->close_count; $i++)
                {
                    if($i < $allQuestions->count())
                        $outputQuestions->push($allQuestions[$i]);
                    else
                        $outputQuestions->push($allQuestions->random());
                }
        }
        $allQuestions = $this->subject->open_questions->load('answers')->shuffle();
        if ($allQuestions->count() != 0) {
            for( $i = 0; $i < $this->open_count; $i++)
                {
                    if($i < $allQuestions->count())
                        $outputQuestions->push($allQuestions[$i]);
                    else
                        $outputQuestions->push($allQuestions->random());
                }
        }
        $allQuestions = $this->subject->map_questions->load('answers')->shuffle();
        if ($allQuestions->count() != 0) {
            for( $i = 0; $i < $this->mapping_count; $i++)
                {
                    if($i < $allQuestions->count())
                        $outputQuestions->push($allQuestions[$i]);
                    else
                        $outputQuestions->push($allQuestions->random());
                }
        }
        $allQuestions = $this->subject->seq_questions->load('answers')->shuffle();
        if ($allQuestions->count() != 0) {
            for( $i = 0; $i < $this->sequence_count; $i++)
                {
                    if($i < $allQuestions->count())
                        $outputQuestions->push($allQuestions[$i]);
                    else
                        $outputQuestions->push($allQuestions->random());
                }
        }
        return $outputQuestions;
    }

    public function getQuestionsCountAttribute()
    {
        return  ($this->subject->close_questions()->count() != 0 ? $this->close_count    : 0) +
                ($this->subject->open_questions()->count() != 0 ? $this->open_count      : 0) +
                ($this->subject->map_questions()->count() != 0 ? $this->mapping_count    : 0) +
                ($this->subject->seq_questions()->count() != 0 ? $this->sequence_count   : 0);
    }

    /**
     * Check access level for user (Admin or Teacher)
     * @param user (Admin or Teahcer)
     * @return true or false
     *
     **/
    public function isEditableBy($user)
    {
        return (($this->user_id == $user->id) && $user->isTeacher()) || $user->isAdmin();
    }

    /**
     * Activate this Exam for students
     * Activate this Exam for students and save it (already has ->save() method inside)
     * @return true if success or false if failed
     **/
    public function activate()
    {
        try
        {
            $this->is_active=true;
            $this->save();
            return true;
        }
        catch (Exception $ex)
        {
            return false;
        }
    }

    /**
     * DEActivate this Exam for students
     * DEActivate this Exam for students and save it (already has ->save() method inside)
     * @return true if success or false if failed
     **/
    public function deactivate()
    {
        try
        {
            $this->is_active=false;
            $this->save();
            return true;
        }
        catch (Exception $ex)
        {
            return false;
        }
    }

    /**
     * Check to last try student of pass this exam
     * @param $user
     * @return bool
     **/
    public function passedBy($user)
    {
        $completedExamsCount = DB::table('stats')
            ->where('user_id',$user->id)
            ->where('exam_id',$this->id)
            ->where('is_completed',true)
            ->where('is_training', 0)
            ->count();
        $unCompletedExamsCount = DB::table('stats')
            ->where('user_id',$user->id)
            ->where('exam_id',$this->id)
            ->where('is_completed',false)
            ->count();
        if(($completedExamsCount==0 && $unCompletedExamsCount==0)||$unCompletedExamsCount!=0)
            return false;
        else
            return true;
    }

    public function statistics()
    {
        return $this->hasMany('App\Statistic','exam_id')->where('is_completed',true);
    }

    public static function forUser(\App\User $user)
    {
        if (!$user->isAdmin()) {
            return Exam::where('user_id', $user->id)->get();
        }
        return Exam::all();
    }
}