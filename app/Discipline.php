<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Discipline
 *
 * @property-read \App\Specialty $specialty
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Subject[] $subjects
 * @mixin \Eloquent
 */
class Discipline extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'specialty_id',
    ];

    protected $table = 'disciplines';

    public $timestamps = false;

    public function subjects()
    {
        return $this->hasMany('App\Subject', 'discipline_id');
    }

    public function specialty()
    {
        return $this->belongsTo('App\Specialty', 'specialty_id');
    }
}
