<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Statistic
 *
 * @property-read \App\Exam $exam
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Question[] $examQuestions
 * @property-read mixed $earned_points
 * @property-read float $points_percent
 * @property-read mixed $total_points
 * @property-read \App\User $user
 * @mixin \Eloquent
 */
class Statistic extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'exam_id', 'user_id', 'is_completed', 'is_training',
    ];

    protected $appends = [
        'earnedPoints', 'totalPoints', 'pointsPercent',
    ];

    protected $table = 'stats';

    /**
     * get exam which were stats
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo exam instance
     */
    public function exam()
    {
        return $this->belongsTo('App\Exam','exam_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function examQuestions()
    {
        return $this->belongsToMany('App\Question','exam_questions','stat_id','question_id')
            ->withPivot('is_passed');
    }

    public function completeTesting()
    {
        $this->is_completed = true;
        $this->save();
    }

    /**
     * Get test results
     * @return float
     */
    public function getPointsPercentAttribute()
    {
        $earnedPoints = $this->earnedPoints;
        $totalPoints = $this->totalPoints;
        return round(($earnedPoints/$totalPoints)*100,2);
    }

    public function getTotalPointsAttribute()
    {
        return count($this->examQuestions->where('type','selectable'))*1
            + count($this->examQuestions->where('type','open'))*2
            + count($this->examQuestions->where('type','mapping'))*3
            + count($this->examQuestions->where('type','sequence'))*4;
    }

    public function getEarnedPointsAttribute()
    {
        $passedSelectableCount = count($this->examQuestions()->where('type','selectable')->wherePivot('is_passed',true)->get());
        $passedOpenCount = count($this->examQuestions()->where('type','open')->wherePivot('is_passed',true)->get());
        $passedMappingCount = count($this->examQuestions()->where('type','mapping')->wherePivot('is_passed',true)->get());
        $passedSequenceCount = count($this->examQuestions()->where('type','sequence')->wherePivot('is_passed',true)->get());

        return $passedSelectableCount*1
            + $passedOpenCount*2
            + $passedMappingCount*3
            + $passedSequenceCount*4;
    }
}
