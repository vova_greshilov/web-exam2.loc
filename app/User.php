<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App;

/**
 * App\User
 *
 * @property-read \App\Group $group
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Statistic[] $studentStats
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Statistic[] $teacherStats
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'access', 'group_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function group()
    {
        return $this->belongsTo('App\Group');
    }

    public function specialty()
    {
        return $this->group->specialty();
    }

    public function stats()
    {
        switch ($this->access) {
            case 'admin': return $this->adminStats();
            case 'teacher': return $this->teacherStats();
            case 'student': return $this->studentStats();
        }
    }

    public function studentStats()
    {
        return $this->hasMany('App\Statistic','user_id')->where('is_completed',true)->orderBy('created_at','desc');
    }

    public function teacherStats()
    {
        return $this->hasManyThrough('App\Statistic','App\Exam')
            ->orderBy('created_at','desc')
            ->where('is_training',false);
    }

    public function adminStats()
    {
        return App\Statistic::where('is_training',false)
            ->orderBy('created_at','desc');
    }

    public function isAdmin()
    {
        return $this->access==='admin';
    }

    public function isTeacher()
    {
        return $this->access==='teacher';
    }

    public function isStudent()
    {
        return $this->access==='student';
    }

    public function isDisabled()
    {
        return $this->access==='disabled';
    }
}