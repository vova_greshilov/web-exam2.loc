<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Specialty
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Discipline[] $disciplines
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Group[] $groups
 * @mixin \Eloquent
 */
class Specialty extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    protected $table = 'specialties';

    public $timestamps = false;

    public function disciplines()
    {
        return $this->hasMany('App\Discipline','specialty_id');
    }

    public function groups()
    {
        return $this->hasMany('App\Group','specialty_id');
    }
}
