<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

/**
 * App\Subject
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Question[] $close_questions
 * @property-read \App\Discipline $discipline
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Question[] $map_questions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Question[] $open_questions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Question[] $seq_questions
 * @mixin \Eloquent
 */
class Subject extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'discipline_id',
    ];

    public $timestamps = false;

    protected $table = 'subjects';

    public function isBelongsTo(\App\User $user)
    {
        return $this->user_id == $user->id;
    }

    public function discipline()
    {
        return $this->belongsTo('App\Discipline', 'discipline_id');
    }

    public function close_questions()
    {
        return $this->hasMany('App\Question','subject_id')->where('type', 'selectable');
    }

    public function open_questions()
    {
        return $this->hasMany('App\Question','subject_id')->where('type', 'open');
    }

    public function map_questions()
    {
        return $this->hasMany('App\Question','subject_id')->where('type', 'mapping');
    }

    public function seq_questions()
    {
        return $this->hasMany('App\Question','subject_id')->where('type', 'sequence');
    }

    public static function getSubjectsFor(\App\User $user)
    {
        if (!$user->isAdmin()) {
            return Subject::where('user_id', $user->id)->get();
        }
        return Subject::all();
    }
}
