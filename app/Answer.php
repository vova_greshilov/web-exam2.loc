<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Answer
 *
 * @property-read \App\Question $question
 * @mixin \Eloquent
 */
class Answer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'text', 'is_right', 'question_id',
    ];

    protected $table = 'answers';

    protected $hidden = [
        'is_right',
    ];

    public $timestamps = false;

    public function question()
    {
        return $this->belongsTo('App\Question','question_id');
    }
}
