<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * App\Group
 *
 * @property-read \App\Specialty $specialty
 * @mixin \Eloquent
 */
class Group extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'specialty_id',
    ];

    protected $table = 'groups';

    public $timestamps = false;

    public function specialty()
    {
        return $this->belongsTo('App\Specialty');
    }
}
