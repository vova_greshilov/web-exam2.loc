<?php

namespace App\Http\Controllers;

use App\Group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class GroupsController extends Controller
{
    public function ajaxApiGetGroups(Request $request)
    {
        return Response::json(Group::all(),200);
    }

    public function getAdminGroups(Request $request)
    {
        $groups = Group::with('specialty')->paginate(30);
        return view('admin.groups', [
            'groups' => $groups,
        ]);
    }

    public function ajaxAdminCreateGroup(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'specialty_id' => 'required',
        ]);
        $newGroup = Group::create([
            'name' => $request->name,
            'specialty_id' => $request->specialty_id,
        ]);
        $group = Group::with('specialty')->findOrFail($newGroup->id);
        return Response::json($group, 200);
    }

    public function ajaxAdminEditGroup(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'specialty_id' => 'required',
        ]);
        $group = Group::with('specialty')->findOrFail($request->group_id);
        $group->name = $request->name;
        $group->specialty_id = $request->specialty_id;
        $group->save();
        $group = Group::with('specialty')->findOrFail($request->group_id);
        return Response::json($group, 200);
    }

    public function ajaxAdminDeleteGroup(Request $request)
    {
        Group::destroy($request->group_id);
        return Response::json($request->group_id, 200);
    }

    public function ajaxAdminGroup(Request $request)
    {
        $group = Group::findOrFail($request->group_id);
        return Response::json($group, 200);
    }
}
