<?php

namespace App\Http\Controllers;

use App\Statistic;
use Illuminate\Http\Request;

class StatController extends Controller
{
    public function getUserStats(Request $request)
    {
        $user = $request->user();
        $stats = $user->stats();
        $stats = $stats->with('exam.subject.discipline.specialty');
        $stats = $stats->with('user');

        if (isset($request->filter_specialty) && $request->filter_specialty != 'all') {
            $stats = $stats->whereHas('exam.subject.discipline', function ($query) use ($request) {
                $query->where('specialty_id', $request->filter_specialty);
            });
        }
        if (isset($request->filter_discipline) && $request->filter_discipline != 'all') {
            $stats = $stats->whereHas('exam.subject', function ($query) use ($request) {
                $query->where('discipline_id' ,$request->filter_discipline);
            });
        }
        if (isset($request->filter_subject) && $request->filter_subject != 'all') {
            $stats = $stats->whereHas('exam', function ($query) use ($request) {
                $query->where('subject_id', $request->filter_subject);
            });

        }
        $stats = $stats->paginate(30);
        return view('stats', [
        'stats' => $stats,
        ]);
    }

    public function getUserStat(Request $request)
    {
        $stat = Statistic::with('examQuestions')
            ->with('exam.subject.discipline.specialty')
            ->with('user')
            ->findOrFail($request->stat_id);
        return view('stat', [
            'stat' => $stat,
        ]);
    }
}
