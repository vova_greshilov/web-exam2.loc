<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class UsersController extends Controller
{
    public function getAdminUsers(Request $request)
    {
        $users = User::with('group')->orderBy('created_at', 'desc');
        if($request->filter_group && (int)$request->filter_group)
        {
            $users = $users->where('group_id', $request->filter_group);
        }
        $users = $users ->paginate(10);
        return view('admin.users', [
            'users' => $users,
        ]);
    }

    public function ajaxAdminEditUser(Request $request)
    {
        $this->validate($request, [
            'access' => 'required',
            'user_id' => 'required',
        ]);
        $user = User::findOrFail($request->user_id);
        $user->access = $request->access;
        $user->save();
        return Response::json($user, 200);
    }

    public function ajaxAdminDeleteUser(Request $request, $userId)
    {
        User::destroy($userId);
        return redirect()->back();
    }
}
