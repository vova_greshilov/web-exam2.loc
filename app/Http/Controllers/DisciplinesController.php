<?php

namespace App\Http\Controllers;

use App\Discipline;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class DisciplinesController extends Controller
{
    public function ajaxApiGetDisciplines(Request $request)
    {
        if (isset($request->specialty_id) && $request->specialty_id != 'all') {
            return Response::json(Discipline::where('specialty_id', $request->specialty_id)->get(),200);
        } else {
            return Response::json(Discipline::all(),200);
        }
    }

    public function getAdminDisciplines(Request $request)
    {
        $disciplines = Discipline::with('specialty');
        if($request->filter_specialty && (int)$request->filter_specialty)
        {
            $disciplines = $disciplines->where('specialty_id', $request->filter_specialty);
        }
        $disciplines = $disciplines ->paginate(10);
        return view('admin.disciplines', [
            'disciplines' => $disciplines,
        ]);
    }

    public function ajaxAdminCreateDiscipline(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'specialty_id' => 'required',
        ]);
        $newDiscipline = Discipline::create([
            'name' => $request->name,
            'specialty_id' => $request->specialty_id,
        ]);
        $discipline = Discipline::with('specialty')->findOrFail($newDiscipline->id);
        return Response::json($discipline, 200);
    }

    public function ajaxAdminEditDiscipline(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'specialty_id' => 'required',
        ]);
        $discipline = Discipline::with('specialty')->findOrFail($request->discipline_id);
        $discipline->name = $request->name;
        $discipline->specialty_id = $request->specialty_id;
        $discipline->save();
        $discipline = Discipline::with('specialty')->findOrFail($request->discipline_id);
        return Response::json($discipline, 200);
    }

    public function ajaxAdminDeleteDiscipline(Request $request)
    {
        Discipline::destroy($request->discipline_id);
        return Response::json($request->discipline_id, 200);
    }

    public function ajaxAdminDiscipline(Request $request)
    {
        $discipline = Discipline::findOrFail($request->discipline_id);
        return Response::json($discipline, 200);
    }
}
