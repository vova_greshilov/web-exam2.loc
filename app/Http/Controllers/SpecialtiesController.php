<?php

namespace App\Http\Controllers;

use App\Specialty;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class SpecialtiesController extends Controller
{
    public function ajaxApiGetSpecialties(Request $request)
    {
        return Response::json(Specialty::all(), 200);
    }

    public function getAdminSpecialties(Request $request)
    {
        $specs = Specialty::paginate(30);
        return view('admin.specialties', [
            'specs' => $specs,
        ]);
    }

    public function ajaxAdminCreateSpecialty(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:100',
        ]);
        $newSpec = Specialty::create([
            'name' => $request->name,
        ]);
        return Response::json($newSpec, 200);
    }

    public function ajaxAdminSpecialty(Request $request)
    {
        $spec = Specialty::findOrFail($request->spec_id);
        return Response::json($spec, 200);
    }

    public function ajaxAdminEditSpecialty(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:100',
        ]);
        $spec = Specialty::findOrFail($request->spec_id);
        $spec->name = $request->name;
        $spec->save();
        return Response::json($spec, 200);
    }

    public  function ajaxAdminDeleteSpecialty(Request $request)
    {
        Specialty::destroy($request->spec_id);
        return Response::json($request->spec_id, 200);
    }
}
