<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Exam;
use App\ExamQuestion;
use App\Question;
use App\Statistic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class ExamController extends Controller
{
    public function getTeacherExams(Request $request)
    {
        if (!Auth::user()->isAdmin()) {
            $exams = Exam::where('user_id', Auth::user()->id);
        } else {
            $exams = Exam::with('creator');
        }
        if (isset($request->filter_specialty) && $request->filter_specialty != 'all') {
            $exams = $exams->whereHas('subject.discipline', function ($query) use ($request) {
                $query->where('specialty_id', $request->filter_specialty);
            });
        }
        if (isset($request->filter_discipline) && $request->filter_discipline != 'all') {
            $exams = $exams->whereHas('subject', function ($query) use ($request) {
                $query->where('discipline_id' ,$request->filter_discipline);
            });
        }
        if (isset($request->filter_subject) && $request->filter_subject != 'all') {
            $exams = $exams->where('subject_id', $request->filter_subject);
        }
        $exams = $exams->paginate(30);
        return view('teacher.exams', [
            'exams' => $exams,
        ]);
    }

    public function ajaxTeacherCreateExam(Request $request)
    {
        $this->validate($request, [
            "subject_id" => "required|integer",
            "questionCount" => "required|integer",
            "open_questions_count" => "required|integer",
            "mapping_questions_count" => "required|integer",
            "sequence_questions_count" => "required|integer",
        ]);
        $newExam = Exam::create([
            'close_count'=> $request->questionCount,
            'open_count'=> $request->open_questions_count,
            'sequence_count'=> $request->mapping_questions_count,
            'mapping_count'=> $request->sequence_questions_count,
            'is_active'=> true,
            'subject_id'=> $request->subject_id,
            'user_id' => Auth::user()->id,
        ]);
        $exam = Exam::with('subject')
            ->with('subject.discipline')
            ->with('subject.discipline.specialty')
            ->findOrfail($newExam->id);
        return Response::json($exam, 200);
    }

    public function ajaxTeacherDeleteExam(Request $request)
    {
        Exam::destroy($request->exam_id);
        return Response::json($request->exam_id);
    }

    public function ajaxTeacherEditExam(Request $request)
    {
        $exam = Exam::findOrFail($request->exam_id);
        $exam->is_active = !$exam->is_active;
        $exam->save();
        return Response::json($exam, 200);
    }

    public function getStudentExams(Request $request)
    {
        $exams = Exam::whereHas('subject.discipline',function ($query) {
            $query->where('specialty_id',Auth::user()->specialty->id);})
            ->where('is_active',true)
            ->with('creator')
            ->with('subject')
            ->get();
        return view('student.exams',[
            'exams' => $exams,
        ]);
    }

    public function getStudentExamForm(Request $request)
    {
        $exam = Exam::findOrFail($request->exam_id);
        if ($exam->passedBy($request->user()))
            return redirect()->back();
        $stat = Statistic::where('user_id', Auth::user()->id)
            ->with('exam')
            ->with('exam.subject')
            ->with('exam.subject.discipline')
            ->where('exam_id',$exam->id)
            ->where('is_completed', false)
            ->get();
        session_start();

        if(count($stat)==0) {
            //it is first attempt
            $stat = Statistic::create([
                'user_id' => Auth::user()->id,
                'exam_id' => $exam->id,
                'is_completed' => false,
                'is_training' => isset($request->training),
            ]);
            /* set up session */
            $_SESSION['stat_id'] = $stat->id;
            $_SESSION['questions'] = $stat->exam->questions();
            $_SESSION['currentQuestion'] = 0;
        } else {
            $stat = $stat->first();
            if(isset($_SESSION['stat_id'])) {
                //it is "page reload" case
                $_SESSION['currentQuestion'] = $_SESSION['currentQuestion'] - 1;
                if ($_SESSION['stat_id'] != $stat->id) {
                    //if it is another one exam, force complete previous test
                    $this->completeTesting($_SESSION['stat_id']);
                    /* set up session */
                    $_SESSION['stat_id'] = $stat->id;
                    $_SESSION['questions'] = $stat->exam->questions();
                    $_SESSION['currentQuestion'] = 0;
                }
            } else {
                //its a second attempt to pass exam
                $_SESSION['stat_id'] = $stat->id;
                $_SESSION['questions'] = $stat->exam->questions();
                $_SESSION['currentQuestion'] = 0;
            }
        };
        return view('student.exam', [
            'stat' => $stat,
        ]);
    }


    public function completeTesting($stat_id)
    {
        $stat = Statistic::findOrfail($_SESSION['stat_id']);
        $stat->completeTesting();
        unset($_SESSION['stat_id']);
        unset($_SESSION['questions']);
        unset($_SESSION['currentQuestion']);
    }

    public function ajaxGetExamQuestion(Request $request)
    {
        session_start();
        if ($request->stat_id != $_SESSION['stat_id']) {
            $this->completeTesting($request->stat_id);
            return Response::json('wrong stat_id', 403);
        }
        if (isset($_SESSION['questions']) && isset($_SESSION['currentQuestion'])) {
            $questions = $_SESSION['questions'];
            $currentQuestion = $_SESSION['currentQuestion'];
            if ($currentQuestion < count($questions)) {
                $_SESSION['currentQuestion'] = $currentQuestion + 1;
                return Response::json([
                    'index' => $currentQuestion,
                    'question' => $questions[$currentQuestion]
                ], 200);
            } else {
                $this->completeTesting($_SESSION['stat_id']);
                return Response::json([
                    'redirectUrl' => route('getUserStat', ['stat_is' => $request->stat_id])
                ], 303);
            }
        } else {
            return Response::json('there is no questions for this session',404);
        }
    }

    public function ajaxStudentConfirmAnswer(Request $request)
    {
        session_start();
        if ($request->stat_id != $_SESSION['stat_id']) {
            $this->completeTesting($request->stat_id);
            return Response::json('wrong stat_id', 403);
        }
        $is_right = false;
        $stat = Statistic::findOrFail($_SESSION['stat_id']);
        $question = Question::findOrFail($request->question_id);
        switch ($question->type) {
            case 'selectable':
                $userAnswers = Answer::find($request->answers);
                $is_right = $userAnswers == $question->rightAnswers;
                break;
            case 'open':
                $answers = $question->answers->map(function ($item) {
                    $temp = mb_strtolower($item->text);
                    $temp = trim($temp);
                    return $temp;
                });
                $userAnswer = mb_strtolower($request->answer);
                $userAnswer = trim($userAnswer);
                $is_right = $answers->contains($userAnswer);
                break;
            case 'sequence':
            case 'mapping':
                $answers = $question->answers->map(function ($item) {
                    $temp = mb_strtolower($item->text);
                    $temp = str_replace(' ','', $temp);
                    $temp = str_replace('-','', $temp);
                    $temp = str_replace(',','', $temp);
                    return $temp;
                });
                $userAnswer = mb_strtolower($request->answer);
                $userAnswer = str_replace(' ','', $userAnswer);
                $userAnswer = str_replace('-','', $userAnswer);
                $userAnswer = str_replace(',','', $userAnswer);
                $is_right = $answers->contains($userAnswer);
                break;
        }
        ExamQuestion::create([
            'is_passed' => $is_right,
            'question_id' => $question->id,
            'stat_id' => $_SESSION['stat_id'],
        ]);
        if ($stat->is_training) {

            return Response::json([
                'is_right' => $is_right,
                'answerText' => $question->rightAnswers->first()->text,
            ], 200);
        }
        return Response::json('passed', 200);
    }
}