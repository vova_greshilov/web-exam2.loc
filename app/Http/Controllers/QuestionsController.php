<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use SimpleExcel\SimpleExcel;

class QuestionsController extends Controller
{
    public function getTeacherQuestions(Request $request)
    {
        if (!Auth::user()->isAdmin()) {
            $questions = Question::where('user_id', Auth::user()->id);
        } else {
            $questions = Question::with('autor');
        }
        if (isset($request->filter_specialty) && $request->filter_specialty != 'all') {
            $questions = $questions->whereHas('subject.discipline', function ($query) use ($request) {
                $query->where('specialty_id', $request->filter_specialty);
            });
        }
        if (isset($request->filter_discipline) && $request->filter_discipline != 'all') {
            $questions = $questions->whereHas('subject', function ($query) use ($request) {
                $query->where('discipline_id' ,$request->filter_discipline);
            });
        }
        if (isset($request->filter_subject) && $request->filter_subject != 'all') {
            $questions = $questions->where('subject_id', $request->filter_subject);
        }
        $questions = $questions->paginate(30);
        return view('teacher.questions', [
            'questions' => $questions,
        ]);
    }

    public function ajaxTeacherQuestion(Request $request)
    {
        $question = Question::with('answers')->with('subject')->findOrFail($request->question_id);
        $question->answers->each(function ($answer) {
            $answer->makeVisible('is_right');
        });
        return Response::json($question);
    }

    public function ajaxTeacherCreateQuestion(Request $request)
    {
        $this->validate($request, [
            "subject_id" => "required|integer",
            "type" => "required",
            "text" => "required",
            "answers" => "required|array",
        ]);
        if ($request->hasFile('img'))
        {
            $file = $request->file('img');
            $image_name = time()."-".$file->getClientOriginalName();
            $file->move('img\uploads', $image_name);
        }
        $newQuestion = Question::create([
            "subject_id" => $request->subject_id,
            "type" => $request->type,
            "text" => $request->text,
            "img" => isset($image_name) ? '/img/uploads/'.$image_name : null,
            'user_id' => Auth::user()->id,
        ]);
        foreach ($request->answers as $answerData) {
            if (!isset($answerData['text'])) {
                return Response::json([
                    'answers.text' => ['Поле текст ответа обязательно']
                ], 422);
            }
            $answer = Answer::create([
                'text' => $answerData['text'],
                'is_right' => isset($answerData['is_right']),
                'question_id' => $newQuestion->id,
            ]);
        }
        $question = Question::with('subject')->findOrfail($newQuestion->id);
        return Response::json($question, 200);
    }

    public function ajaxTeacherEditQuestion(Request $request)
    {
        $this->validate($request, [
            'question_id' => "required|integer",
            "type" => "required",
            "text" => "required",
            "answers" => "required|array",
        ]);
        if ($request->hasFile('img'))
        {
            $file = $request->file('img');
            $image_name = time()."-".$file->getClientOriginalName();
            $file->move('img/uploads', $image_name);
        }
        $question = Question::findOrFail($request->question_id);
        $question->text = $request->text;
        $question->type = $request->type;
        $question->img = isset($image_name) ? '/img/uploads/'.$image_name : null ;
        $question->save();
        foreach ($request->answers as $answerData) {
            if (!isset($answerData['text'])) {
                return Response::json([
                    'answers.text' => ['Поле текст ответа обязательно']
                ], 422);
            }
            $answer = Answer::updateOrCreate([
                'id' => $answerData['id'],
                'question_id' => $question->id,
            ], [
                'text' => $answerData['text'],
                'is_right' => isset($answerData['is_right']),
            ]);
        }
        return Response::json($question, 200);

    }

    public function ajaxTeacherDeleteQuestion(Request $request)
    {
        Question::destroy($request->question_id);
        return Response::json($request->question_id, 200);
    }

    public function ajaxTeacherDeleteAnswer(Request $request)
    {
        Answer::destroy($request->answer_id);
        return Response::json($request->answer_id);
    }


    public function importQuestions(Request $request)
    {
        $userId = $request->user()->id;
        $subjectId = $request->subject_id;
        //Получить файл
        $file = $request->file('file');
        $fileName = public_path('import');
        //Переместить файл на сайт
        $file->move($fileName, $file->getClientOriginalName());
        $fileName = $fileName . '/' .$file->getClientOriginalName();
        //Открыть парсером
        $excel = new SimpleExcel($file->getClientOriginalExtension());
        $excel->parser->loadFile($fileName);

        $data = [
            "type" => '',
            "text" => '',
            "answers" => array(),
        ];
        for ($i = 2; $i < count($excel->parser->getField()); $i++) {
            $row = $excel->parser->getRow($i);
            if ($row[0] !== '') {
                if($data['text'] !== '') {
                    $this->addQuestion($data, $subjectId, $userId);
                }
                $data['type'] = $this->getTypeByString($row[0]);
                $data['text'] = $row[1];
                $data['answers'] = array();
            }
            array_push($data['answers'], [
                'text' => $row[2],
                'is_right' => $row[3] !== '-'
            ]);
        }
        return redirect()->action('QuestionsController@getTeacherQuestions', ['filter_subject' => $subjectId]);
    }

    protected function getTypeByString($var)
    {
        switch ($var) {
            case 'открытый':
            case 'ввод':
            case 'ввод правильного ответа':
                return 'open';
            case 'выбор':
            case 'закрытый':
            case 'выбор правильного варианта':
            case 'с выбором правильного варианта':
                return 'selectable';
            case 'последовательность':
            case 'выстраивание последовательности':
            case 'составить последовательность':
                return 'sequence';
            case 'сопоставление вариантов':
            case 'сопоставление':
            case 'на сопоставление':
                return 'mapping';
        }
    }

    protected function addQuestion($data, $subjectId, $userId)
    {
        $newQuestion = Question::create([
            "subject_id" => $subjectId,
            "type" => $data['type'],
            "text" => $data['text'],
            "img" => null,
            'user_id' => $userId,
        ]);
        foreach ($data['answers'] as $answerData) {
            Answer::create([
                'text' => $answerData['text'],
                'is_right' => $answerData['is_right'],
                'question_id' => $newQuestion->id,
            ]);
        }
    }
}
