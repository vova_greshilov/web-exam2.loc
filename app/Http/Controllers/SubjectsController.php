<?php

namespace App\Http\Controllers;

use App\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class SubjectsController extends Controller
{
    public function getTeacherSubjects(Request $request)
    {
        $subjects = Subject::with('discipline')->with('discipline.specialty');
        if (isset($request->filter_specialty) && $request->filter_specialty != 'all') {
            $subjects = $subjects->whereHas('discipline', function ($query) use ($request) {
                $query->where('specialty_id', $request->filter_specialty);
            });
        }
        if (isset($request->filter_discipline) && $request->filter_discipline != 'all') {
            $subjects = $subjects->where('discipline_id' ,$request->filter_discipline);
        }
        if (!$request->user()->isAdmin()) {
            $subjects = $subjects->where('user_id', $request->user()->id);
        }
        $subjects = $subjects->paginate(30);
        return view('teacher.subjects', [
            'subjects' => $subjects,
        ]);
    }

    public function ajaxApiGetSubjects(Request $request)
    {
        $subjects = Subject::with('discipline')->with('discipline.specialty');
        if (isset($request->discipline_id) && $request->discipline_id != 'all') {
            $subjects = $subjects
                ->where('discipline_id', $request->discipline_id);
        }
        if (!$request->user()->isAdmin()) {
            $subjects = $subjects->where('user_id', $request->user()->id);
        }
        $subjects = $subjects->get();
        return Response::json($subjects, 200);
    }

    public function ajaxTeacherCreateSubject(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'discipline_id' => 'required|integer',
        ]);
        $newSubject = Subject::create([
            'name' => $request->name,
            'discipline_id' => $request->discipline_id,
        ]);
        $subject = Subject::with('discipline')->with('discipline.specialty')->findOrFail($newSubject->id);
        return Response::json($subject, 200);
    }

    public function ajaxTeacherEditSubject(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);
        $subject = Subject::findOrFail($request->subject_id);
        $subject->name = $request->name;
        $subject->save();
        $subject = Subject::findOrFail($request->subject_id);
        return Response::json($subject, 200);
    }

    public function ajaxTeacherSubject(Request $request)
    {
        $subject = Subject::with('discipline')->with('discipline.specialty')->findOrFail($request->subject_id);
        return Response::json($subject);
    }

    public function ajaxTeacherDeleteSubject(Request $request)
    {
        Subject::destroy($request->subject_id);
        return Response::json($request->subject_id, 200);
    }
}
