#!/usr/bin/env bash

BUILD_ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [[ ${BUILD_ROOT_DIR} != *"/build"* ]]
then
  BUILD_ROOT_DIR="$BUILD_ROOT_DIR/build"
fi

ROOT_DIR=$(dirname "${BUILD_ROOT_DIR}")

if [ ! -f ${ROOT_DIR}/composer.phar ]; then
    php -r "readfile('https://getcomposer.org/installer');" | php -- --install-dir=${ROOT_DIR}
fi

php ${ROOT_DIR}/composer.phar install -d ${ROOT_DIR}
php ${ROOT_DIR}/composer.phar dumpautoload

chmod -R 777 ${ROOT_DIR}/storage/
chmod -R 777 ${ROOT_DIR}/bootstrap/cache/

php ${ROOT_DIR}/artisan migrate --force
php ${ROOT_DIR}/artisan view:clear
php ${ROOT_DIR}/artisan cache:clear
php ${ROOT_DIR}/artisan clear-compiled
php ${ROOT_DIR}/artisan optimize

cd ${ROOT_DIR}
mv -f build/XMLParser.php vendor\faisalman\simple-excel-php\src\SimpleExcel\Parser\XMLParser.php

echo "Done..."
