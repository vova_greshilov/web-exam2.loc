<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'Атрибут :attribute должен быть принят.',
    'active_url'           => 'Атрибут :attribute не является правильным URL.',
    'after'                => 'Атрибут :attribute должен быть датой старше :date.',
    'after_or_equal'       => 'Атрибут :attribute должен быть датой :date или старше.',
    'alpha'                => 'Атрибут :attribute должен содержать только буквы.',
    'alpha_dash'           => 'Атрибут :attribute должен содержать только буквы, цифры, и дефисы.',
    'alpha_num'            => 'Атрибут :attribute должен содержать только буквы and цифры.',
    'array'                => 'Атрибут :attribute должен быть массивом.',
    'before'               => 'Атрибут :attribute должен быть датой раньше :date.',
    'before_or_equal'      => 'Атрибут :attribute должен быть датой :date или раньше.',
    'between'              => [
        'numeric' => 'Атрибут :attribute должен быть между :min and :max.',
        'file'    => 'Атрибут :attribute должен быть между :min and :max kilobytes.',
        'string'  => 'Атрибут :attribute должен быть между :min and :max characters.',
        'array'   => 'Атрибут :attribute должен иметь от :min до :max элементов.',
    ],
    'boolean'              => 'Атрибут :attribute должен быть ИСТИНА или ЛОЖЬ.',
    'confirmed'            => 'Атрибут :attribute не совпадает.',
    'date'                 => 'Атрибут :attribute не я является правильной датой.',
    'date_format'          => 'Атрибут :attribute не соответствует формату :format.',
    'different'            => 'Атрибут :attribute и :other должны отличаться.',
    'digits'               => 'Атрибут :attribute должен быть :digits цифр.',
    'digits_between'       => 'Атрибут :attribute должен быть между :min and :max цифр.',
    'dimensions'           => 'Атрибут :attribute имеет неправильное разрешение.',
    'distinct'             => 'Атрибут :attribute имеет дублирующиеся значения.',
    'email'                => 'Атрибут :attribute должен содержать правильный email адрес.',
    'exists'               => 'Выбранный атрибут :attribute неверен.',
    'file'                 => 'Атрибут :attribute должен быть файлом.',
    'filled'               => 'Атрибут :attribute должен содержать значение.',
    'image'                => 'Атрибут :attribute должен быть изображением.',
    'in'                   => 'Выбранный атрибут :attribute неверен.',
    'in_array'             => 'Атрибут :attribute не существует в :other.',
    'integer'              => 'Атрибут :attribute должен быть целым числом.',
    'ip'                   => 'Атрибут :attribute должен быть правильным IP адресом.',
    'json'                 => 'Атрибут :attribute должен быть правильной JSON строкой.',
    'max'                  => [
        'numeric' => 'Атрибут :attribute должен быть не больше чем :max.',
        'file'    => 'Атрибут :attribute должен быть не больше чем :max килобайт.',
        'string'  => 'Атрибут :attribute должен быть не больше чем :max символов.',
        'array'   => 'Атрибут :attribute должен содержать не больше чем :max элементов.',
    ],
    'mimes'                => 'Атрибут :attribute должен быть файлом с типом: :values.',
    'mimetypes'            => 'Атрибут :attribute должен быть файлом с типом: :values.',
    'min'                  => [
        'numeric' => 'Атрибут :attribute должен быть по крайней мере :min.',
        'file'    => 'Атрибут :attribute должен быть по крайней мере :min килобайт.',
        'string'  => 'Атрибут :attribute должен быть по крайней мере :min символов.',
        'array'   => 'Атрибут :attribute должен содержать по крайней мере :min элементов.',
    ],
    'not_in'               => 'Выбранный атрибут :attribute неверен.',
    'numeric'              => 'Атрибут :attribute должен быть числом.',
    'present'              => 'Атрибут :attribute field должен присутствовать.',
    'regex'                => 'Атрибут :attribute формат неверен.',
    'required'             => 'Атрибут :attribute обязателен.',
    'required_if'          => 'Атрибут :attribute обязателен когда :other содержит :value.',
    'required_unless'      => 'Атрибут :attribute обязателен за исключением когда :other находится в :values.',
    'required_with'        => 'Атрибут :attribute обязателен когда :values присутствует.',
    'required_with_all'    => 'Атрибут :attribute обязателен когда :values присутствует.',
    'required_without'     => 'Атрибут :attribute обязателен когда :values не присутствует.',
    'required_without_all' => 'Атрибут :attribute обязателен когда none of :values присутствуют.',
    'same'                 => 'Атрибут :attribute и :other должны совпадать.',
    'size'                 => [
        'numeric' => 'Атрибут :attribute должен быть :size.',
        'file'    => 'Атрибут :attribute должен быть :size килобайт.',
        'string'  => 'Атрибут :attribute должен быть :size символов.',
        'array'   => 'Атрибут :attribute должен содержать :size элементов.',
    ],
    'string'               => 'Атрибут :attribute должен быть строкой.',
    'timezone'             => 'Атрибут :attribute должен быть правильной временной зоной.',
    'unique'               => 'Атрибут :attribute уже занят.',
    'uploaded'             => 'Атрибут :attribute не удалось загрузить.',
    'url'                  => 'Атрибут :attribute имел неверный формат.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
