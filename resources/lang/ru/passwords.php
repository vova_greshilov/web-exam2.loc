<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль должен содержать по крайней мере 6 символов.',
    'reset' => 'Ваш пароль был изменен!',
    'sent' => 'На вашу почту была отправлена ссылка на сброс пароля!',
    'token' => 'Эта ссылка на сброс пароля не действительна.',
    'user' => "Мы не можем найти пользователя с указанны e-mail.",

];
