<section>
    <p>Вы зашли под учетной записью администратора. Вам доступны следующие функции/разделы:</p>
    <ul class="list-group">
        <li class="list-group-item"><a href="{{ route('getAdminUsers')}}">Управление пользователями</a></li>
        <li class="list-group-item"><a href="{{ route('getAdminSpecialties')}}">Управление специальностями</a></li>
        <li class="list-group-item"><a href="{{ route('getAdminGroups')}}">Управление групппами</a></li>
        <li class="list-group-item"><a href="{{ route('getTeacherSubjects')}}">Управление темами</a></li>
        <li class="list-group-item"><a href="{{ route('getTeacherQuestions')}}">Управление вопросами</a></li>
        <li class="list-group-item"><a href="{{ route('getTeacherExams')}}">Управление тестами</a></li>
        <li class="list-group-item"><a href="{{ route('getUserStats')}}">Просмотр результатов</a></li>
    </ul>
</section>