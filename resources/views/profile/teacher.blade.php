<section>
    <p>Вы зашли под учетной записью преподавателя. Вам доступны следующие функции/разделы:</p>
    <ul class="list-group">
        <li class="list-group-item"><a href="{{ route('getTeacherSubjects')}}">Управление темами</a></li>
        <li class="list-group-item"><a href="{{ route('getTeacherQuestions')}}">Управление вопросами</a></li>
        <li class="list-group-item"><a href="{{ route('getTeacherExams')}}">Управление тестами</a></li>
        <li class="list-group-item"><a href="{{ route('getUserStats')}}">Просмотр результатов</a></li>
    </ul>
</section>