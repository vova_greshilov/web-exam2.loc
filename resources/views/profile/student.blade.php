<section>
    <p>Вы зашли под учетной записью студента. Вам доступны следующие функции/разделы:</p>
    <ul class="list-group">
        <li class="list-group-item"><a href="{{ route('getStudentExams')}}">Прохождение тестов</a></li>
        <li class="list-group-item"><a href="{{ route('getUserStats')}}">Просмотр результатов</a></li>
    </ul>
</section>