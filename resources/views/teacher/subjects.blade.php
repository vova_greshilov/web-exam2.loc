@extends('layouts.app')

@section('content')
    <div class="panel-heading">
        <span class="panel-title">Темы</span>
    </div>
    <section class="panel-body">
        <h3>Темы</h3>
        <p><span><a href="#" onclick="showSubjectForm()">Cоздать новую тему</a></span></p>
        <p>Отфильтровать список по параметрам</p>
        <form class='form' action="{{ route('getTeacherSubjects') }}" method="get">
            <div class="form-group">
                <label for="filter_specialty">Специальность</label>
                <select id="filter_specialty" name="filter_specialty" class="form-control filter_specialty" onchange="loadDisciplines(this)" style="width: 100%;">
                    <option value="all">Все</option>
                    @foreach(\App\Specialty::all() as $spec)
                        <option {{Request::input('filter_specialty')==$spec->id ? 'selected' : ''}} value="{{ $spec->id }}">{{$spec->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="filter_discipline">Дисциплина</label>
                <select id="filter_discipline" name="filter_discipline" class="form-control filter_discipline" onchange="" style="width: 100%;">
                    <option value="all">Все</option>
                    @foreach(\App\Discipline::all() as $discipline)
                        <option {{Request::input('filter_discipline')==$discipline->id ? 'selected' : ''}} value="{{ $discipline->id }}">{{$discipline->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <button class="btn btn-default">Обновить</button>
            </div>
        </form>
        <table class="table">
            <thead>
            <tr>
                <th>Название</th>
                <th>Дисциплина</th>
                <th>Специальность</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @if(count($subjects)==0)
                <tr>
                    <td colspan="4">Нет данных</td>
                </tr>
            @endif
            @foreach($subjects as $subject)
                <tr class="subject-{{ $subject->id }}">
                    <td class="name">
                        <p><a href="{{ route('getTeacherQuestions', ['filter_subject' => $subject->id]) }}">{{ $subject->name}}</a></p>
                    </td>
                    <td>
                        <p>{{ $subject->discipline->name }}</p>
                    </td>
                    <td>
                        <p>{{ $subject->discipline->specialty->name }}</p>
                    </td>
                    <td>
                        <span><a href="#" onclick="showSubjectForm({{ $subject->id }})">Редакиторовать</a></span>
                        <span> | </span>
                        <span><a href="#" onclick="deleteSubject({{ $subject->id }})">Удалить</a></span>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if(Request::input('filter_specialty'))
            {{ $subjects->appends(['filter_specialty' => Request::input('filter_specialty')])->links() }}
        @endif
        @if(Request::input('filter_specialty'))
            {{ $subjects->appends(['filter_specialty' => Request::input('filter_specialty')])->links() }}
        @endif
        @if(Request::input('filter_specialty'))
            {{ $subjects->appends(['filter_specialty' => Request::input('filter_specialty')])->links() }}
        @endif
        @if(Request::input('filter_specialty'))
            {{ $subjects->appends(['filter_specialty' => Request::input('filter_specialty')])->links() }}
        @endif
        {{ $subjects->links() }}
    </section>
    <div class="modal fade" id="subject-form-modal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Создание/редактирование темы</h4>
                </div>
                <div class="modal-body">
                    <form id="subject-form">
                        <input type="hidden" class="subject_id" name="subject_id">
                        <div class="form-group">
                            <label>Специальность</label>
                            <select class="form-control filter_specialty" onchange="loadDisciplines(this)" style="width: 100%;">
                                <option value="all">Все</option>
                                @foreach(\App\Specialty::all() as $spec)
                                    <option {{Request::input('filter_specialty')==$spec->id ? 'selected' : ''}} value="{{ $spec->id }}">{{$spec->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Дисциплина</label>
                            <select id='discipline_id' name='discipline_id' class="form-control filter_discipline" onchange="" style="width: 100%;">
                                <option value="all">Все</option>
                            </select>
                            <span id='discipline_id-error' class="label label-danger" style="display: none"></span>
                        </div>
                        <div class="form-group">
                            <label for='name' class="control-label">Название темы:</label>
                            <input id='name' class='form-control'
                                      name='name'
                                      placeholder='Введите название темы'
                                      required
                                      autofocus>
                            <span id='name-error' class="label label-danger" style="display: none"></span>
                        </div>
                    </form>
                    <form class="import-form" style="display: none" enctype="multipart/form-data" method="post" action="{{ action('QuestionsController@importQuestions') }}">
                        {{ csrf_field() }}
                        <input type="hidden" class="subject_id" name="subject_id">
                        <div class="form-group">
                            <label for='file' class="control-label">Загрузить вопросы из .xml (для более подробной информации читайте руководство администратора):</label>
                            <input id='file' class='form-control'
                                   name='file'
                                   type="file"
                                   required>
                            <span id='name-error' class="label label-danger" style="display: none"></span>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-default">Загрузить</button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="saveSubject()">Принять</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                </div>
            </div>
        </div>
    </div>
@endsection