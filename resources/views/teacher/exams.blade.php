@extends('layouts.app')

@section('content')
    <div class="panel-heading">
        <span class="panel-title">Тестирование</span>
    </div>
    <section class="panel-body">
        <h3>Тестирование</h3>
        <p>Отфильтровать список по параметрам</p>
        <form class='form' action="{{ route('getTeacherExams') }}" method="get">
            <div class="form-group">
                <label for="filter_specialty">Специальность</label>
                <select id="filter_specialty" name="filter_specialty" class="form-control filter_specialty" onchange="loadDisciplines(this)" style="width: 100%;">
                    <option value="all">Все</option>
                    @foreach(\App\Specialty::all() as $spec)
                        <option {{Request::input('filter_specialty')==$spec->id ? 'selected' : ''}} value="{{ $spec->id }}">{{$spec->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="filter_discipline">Дисциплина</label>
                <select id="filter_discipline" name="filter_discipline" class="form-control filter_discipline" onchange="loadSubjects(this)" style="width: 100%;">
                    <option value="all">Все</option>
                    @foreach(\App\Discipline::all() as $discipline)
                        <option {{Request::input('filter_discipline')==$discipline->id ? 'selected' : ''}} value="{{ $discipline->id }}">{{$discipline->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="filter_subject">Тема</label>
                <select id="filter_subject" name="filter_subject" class="form-control filter_subject" onchange="changeSubject(this)" style="width: 100%;">
                    <option value="all">Все</option>
                    @foreach(\App\Subject::getSubjectsFor(Auth::user()) as $subject)
                        <option {{Request::input('filter_subject')==$subject->id ? 'selected' : ''}} value="{{ $subject->id }}">{{$subject->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <button class="btn btn-default">Обновить список</button>
            </div>
        </form>
        <div class="form-group">
            <button id='create_btn' class='btn btn-primary' href="#" onclick="showExamForm()">Cоздать тестирование</button>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th>Тема</th>
                <th>Дисциплина</th>
                <th>Специальность</th>
                <th>Создал</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @if(count($exams)==0)
                <tr>
                    <td colspan="2">Нет данных</td>
                </tr>
            @endif
            @foreach($exams as $exam)
                <tr class="exam-{{ $exam->id }} {{ $exam->is_active ? "success" : "active" }}">
                    <td>{{ $exam->subject->name }}</td>
                    <td>{{ $exam->subject->discipline->name }}</td>
                    <td>{{ $exam->subject->discipline->specialty->name }}</td>
                    <td>{{ $exam->creator->name }}</td>
                    <td>
                        <span>
                            <a class='toggle' href="#" onclick="editExam({{ $exam->id }})">
                                {{ $exam->is_active ? "Деактивировать" : "Активировать" }}
                            </a>
                        </span><span> | </span>
                        <span><a href="#" onclick="deleteExam({{ $exam->id }})">Удалить</a></span>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if(Request::input('filter_specialty'))
            {{ $exams->appends(['filter_specialty' => Request::input('filter_specialty')])->links() }}
        @endif
        @if(Request::input('filter_specialty'))
            {{ $exams->appends(['filter_specialty' => Request::input('filter_specialty')])->links() }}
        @endif
        @if(Request::input('filter_specialty'))
            {{ $exams->appends(['filter_specialty' => Request::input('filter_specialty')])->links() }}
        @endif
        @if(Request::input('filter_specialty'))
            {{ $exams->appends(['filter_specialty' => Request::input('filter_specialty')])->links() }}
        @endif
        {{ $exams->links() }}
    </section>
    <div class="modal fade" id="exam-form-modal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Создание/редактирование тестирования</h4>
                </div>
                <div class="modal-body">
                    <form id="exam-form">
                        <div class="form-group">
                            <label for="filter_specialty">Специальность</label>
                            <select id="filter_specialty" name="filter_specialty" class="form-control filter_specialty" onchange="loadDisciplines(this)" style="width: 100%;">
                                <option value="all">Все</option>
                                @foreach(\App\Specialty::all() as $spec)
                                    <option {{Request::input('filter_specialty')==$spec->id ? 'selected' : ''}} value="{{ $spec->id }}">{{$spec->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="filter_discipline">Дисциплина</label>
                            <select id="filter_discipline" name="filter_discipline" class="form-control filter_discipline" onchange="loadSubjects(this)" style="width: 100%;">
                                <option value="all">Все</option>
                                @foreach(\App\Discipline::all() as $discipline)
                                    <option {{Request::input('filter_discipline')==$discipline->id ? 'selected' : ''}} value="{{ $discipline->id }}">{{$discipline->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="filter_subject">Тема</label>
                            <select id="subject_id" name="subject_id" class="form-control filter_subject" style="width: 100%;">
                                <option value="all">Все</option>
                                @foreach(\App\Subject::getSubjectsFor(Auth::user()) as $subject)
                                    <option {{Request::input('filter_subject')==$subject->id ? 'selected' : ''}} value="{{ $subject->id }}">{{$subject->name}}</option>
                                @endforeach
                            </select>
                            <span id='subject_id-error' class="label label-danger" style="display: none"></span>
                        </div>
                        <div class="form-group">
                            <label for="questionCount">Сколько вопросов закрытого типа будет в тесте?
                                <br/><small>(Правильный ответ дает 1 балл)</small></label>
                            <input id='questionCount'
                                   name='questionCount'
                                   type="number"
                                   min='2'
                                   class='form-control'>
                            <span id='questionCount-error' class="label label-danger" style="display: none"></span>
                        </div>
                        <div class="form-group">
                            <label for="open_questions_count">Сколько вопросов открытого типа будет в тесте?
                                <br/><small>(Правильный ответ дает 2 балла)</small></label>
                            <input id='open_questions_count'
                                   name='open_questions_count'
                                   type="number"
                                   min='2'
                                   class='form-control'>
                            <span id='open_questions_count-error' class="label label-danger" style="display: none"></span>
                        </div>
                        <div class="form-group">
                            <label for="mapping_questions_count">Сколько вопросов на сопоставление будет в тесте?
                                <br/><small>(Правильный ответ дает 3 балла)</small></label>
                            <input id='mapping_questions_count'
                                   name='mapping_questions_count'
                                   type="number"
                                   min='2'
                                   class='form-control'>
                            <span id='mapping_questions_count-error' class="label label-danger" style="display: none"></span>
                        </div>
                        <div class="form-group">
                            <label for="sequence_questions_count">Сколько вопросов на составление последовательностей будет в тесте?

                                <br/><small>(Правильный ответ дает 4 балла)</small></label>
                            <input id='sequence_questions_count'
                                   name='sequence_questions_count'
                                   type="number"
                                   min='2'
                                   class='form-control'>
                            <span id='sequence_questions_count-error' class="label label-danger" style="display: none"></span>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="saveExam()">Принять</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                </div>
            </div>
        </div>
    </div>
@endsection