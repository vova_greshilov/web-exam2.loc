@extends('layouts.app')

@section('content')
    <div class="panel-heading">
        <span class="panel-title">Вопросы</span>
    </div>
    <section class="panel-body">
        <h3>Вопросы</h3>
        <p>Отфильтровать список по параметрам</p>
        <form class='form' action="{{ route('getTeacherQuestions') }}" method="get">
            <div class="form-group">
                <label for="filter_specialty">Специальность</label>
                <select id="filter_specialty" name="filter_specialty" class="form-control filter_specialty" onchange="loadDisciplines(this)" style="width: 100%;">
                    <option value="all">Все</option>
                    @foreach(\App\Specialty::all() as $spec)
                        <option {{Request::input('filter_specialty')==$spec->id ? 'selected' : ''}} value="{{ $spec->id }}">{{$spec->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="filter_discipline">Дисциплина</label>
                <select id="filter_discipline" name="filter_discipline" class="form-control filter_discipline" onchange="loadSubjects(this)" style="width: 100%;">
                    <option value="all">Все</option>
                    @foreach(\App\Discipline::all() as $discipline)
                        <option {{Request::input('filter_discipline')==$discipline->id ? 'selected' : ''}} value="{{ $discipline->id }}">{{$discipline->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="filter_subject">Тема</label>
                <select id="filter_subject" name="filter_subject" class="form-control filter_subject" onchange="changeSubject(this)" style="width: 100%;">
                    <option value="all">Все</option>
                    @foreach(\App\Subject::getSubjectsFor(Auth::user()) as $subject)
                        <option {{Request::input('filter_subject')==$subject->id ? 'selected' : ''}} value="{{ $subject->id }}">{{$subject->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <button class="btn btn-default">Обновить список</button>
            </div>
        </form>
        <div class="form-group">
            <button id='create_btn' class='btn btn-primary' disabled href="#" onclick="showQuestionForm()">Cоздать вопрос</button>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th>
                    Вопрос
                </th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @if(count($questions)==0)
                <tr>
                    <td colspan="2">Нет данных</td>
                </tr>
            @endif
            @foreach($questions as $question)
                <tr class="question-{{ $question->id }}">
                    <td>
                        <span>
                            @if($question->type == 'open')
                                <code class="label label-success">Ввод правильного ответа</code>
                            @elseif($question->type == 'selectable')
                                <code class="label label-success">Выбор правильного ответа</code>
                            @elseif($question->type == 'sequence')
                                <code class="label label-success">Выстраивание последовательности</code>
                            @elseif($question->type == 'mapping')
                                <code class="label label-success">Сопоставление вариантов</code>
                            @endif
                        </span>
                        <span class="text">
                            {{ ' '.$question->text}}
                        </span>
                    </td>
                    <td>
                        <span><a href="#" onclick="showQuestionForm({{ $question->id }})">Редакиторовать</a></span>
                        <span> | </span>
                        <span><a href="#" onclick="deleteQuestion({{ $question->id }})">Удалить</a></span>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if(Request::input('filter_specialty'))
            {{ $questions->appends(['filter_specialty' => Request::input('filter_specialty')])->links() }}
        @endif
        @if(Request::input('filter_specialty'))
            {{ $questions->appends(['filter_specialty' => Request::input('filter_specialty')])->links() }}
        @endif
        @if(Request::input('filter_specialty'))
            {{ $questions->appends(['filter_specialty' => Request::input('filter_specialty')])->links() }}
        @endif
        @if(Request::input('filter_specialty'))
            {{ $questions->appends(['filter_specialty' => Request::input('filter_specialty')])->links() }}
        @endif
        {{ $questions->links() }}
    </section>
    <div class="modal fade" id="question-form-modal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Создание/редактирование вопроса</h4>
                </div>
                <div class="modal-body">
                    <form id="question-form">
                        <input type="hidden" id="question_id" name="question_id">
                        <div class="form-group">
                            <label for="subject_id">Тема</label>
                            <select id="subject_id" name="subject_id" class="form-control filter_subject" style="width: 100%;">
                            </select>
                            <span id='subject_id-error' class="label label-danger" style="display: none"></span>
                        </div>
                        <div class="form-group">
                            <label for="type">Тип вопроса</label>
                            <select class="form-control" name="type" id="type">
                                <option value="open">"Открытый" (ручной ввод ответа)</option>
                                <option value="selectable">Выбор правильного варианта</option>
                                <option value="sequence">Выстраивание последовательности</option>
                                <option value="mapping">Сопоставление</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for='text' class="control-label">Текст вопроса:</label>
                            <textarea id='text' class='form-control'
                                      name='text'
                                      placeholder='Введите текст вопроса'
                                      required
                                      autofocus></textarea>
                            <span id='text-error' class="label label-danger" style="display: none"></span>
                        </div>
                        <div class="form-group">
                            <label for='img' class="control-label">Текущее изображение для вопроса:</label>
                            <img alt='Изображение недоступно' src="" class="img-preview img-responsive img-rounded">
                        </div>
                        <div class="form-group">
                            <label for='img' class="control-label">Изображение для вопроса (не обязательно):</label>
                            <input id='img'
                                   type='file'
                                   name='img'>
                            <span id='img-error' class="label label-danger" style="display: none"></span>
                        </div>
                        <div class="form-group">
                            <p>Ответы <a href='#' onclick='addAnswer()' class='btn btn-primary'>Добавить ответ</a></p>
                            <div class="form-group answers-group">
                            </div>
                            <span id='answers-error' class="label label-danger" style="display: none"></span>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="saveQuestion()">Принять</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                </div>
            </div>
        </div>
    </div>
@endsection