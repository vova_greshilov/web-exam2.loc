@extends('layouts.app')

@section('content')
    <div class="panel-heading">
        <span class="panel-title">Панель управления</span>
    </div>
    <section class="panel-body">
        <h3>Дисциплины</h3>
        <p>Вы можете <span><a href="#" onclick="showDisciplineForm()">создать дисциплину</a></span></p>
        <table class="table">
            <thead>
            <tr>
                <th>Дисциплина</th>
                <th>
                    <form action="{{ route('getAdminDisciplines') }}" method="get">
                        <div class="form-group">
                            <select onchange="this.form.submit()" name="filter_specialty" class="form-control" id="filter_specialty" style="width: 100%;">
                                <option>Специальность</option>
                                @foreach(\App\Specialty::all() as $spec)
                                    <option {{Request::input('filter_specialty')==$spec->id ? 'selected' : ''}} value="{{ $spec->id }}">{{$spec->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </form>
                </th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @if(count($disciplines)==0)
                <tr>
                    <td colspan="3">Нет данных</td>
                </tr>
            @endif
            @foreach($disciplines as $discipline)
                <tr class="discipline-{{ $discipline->id }}">
                    <td class="name">{{ $discipline->name }}</td>
                    <td class="specialty">{{ $discipline->specialty->name}}</td>
                    <td>
                        <span><a href="#" onclick="showEditDisciplineForm({{ $discipline->id }})">Редакиторовать</a></span>
                        <span> | </span>
                        <span><a href="#" onclick="deleteDiscipline({{ $discipline->id }})">Удалить</a></span>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if(Request::input('filter_specialty'))
            {{ $disciplines->appends(['filter_specialty' => Request::input('filter_specialty')])->links() }}
        @else
            {{ $disciplines->links() }}
        @endif
    </section>
    <div class="modal fade" id="discipline-form-modal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Дисципла</h4>
                </div>
                <div class="modal-body">
                    <form id="discipline-form">
                        <input type="hidden" id="discipline_id" name="discipline_id">
                        <div class="form-group">
                            <label for="specialty_id">Специальность:</label>
                            <select name="specialty_id" class="form-control" id="specialty_id" style="width: 100%;">
                                <option></option>
                            </select>
                            <span id='specialty_id-error' class="label label-danger" style="display: none"></span>
                        </div>
                        <div class="form-group">
                            <label for="name">Название дисциплины:</label>
                            <input type="text" class="form-control" id="name" name="name" required placeholder="Введите название дисциплины">
                            <span id='name-error' class="label label-danger" style="display: none"></span>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="saveDiscipline()">Принять</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                </div>
            </div>
        </div>
    </div>
@endsection