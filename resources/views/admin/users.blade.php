@extends('layouts.app')

@section('content')
    <div class="panel-heading">Пользователи</div>
    <div class="panel-body">
        <table class="table table-condensed">
            <thead>
            <tr>
                <th>ФИО</th>
                <th>E-mail</th>
                <th>
                    <form action="{{ route('getAdminUsers') }}" method="get">
                        <div class="form-group">
                            <select onchange="this.form.submit()" name="filter_group" class="form-control" id="filter_group" style="width: 100%;">
                                <option>Группа</option>
                                @foreach(\App\Group::all() as $group)
                                    <option {{Request::input('filter_group')== $group->id ? 'selected' : ''}} value="{{ $group->id }}">{{$group->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </form>
                </th>
                <th>Права доступа</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @if(count($users)==0)
                <tr>
                    <td colspan="3">Нет данных</td>
                </tr>
            @endif
            @foreach ($users as $user)
                <tr class="user-{{ $user->id }}">
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ ($user->isStudent() || $user->isDisabled())  ? $user->group->name : 'ПРЕПОДАВАТЕЛЬ' }}</td>
                    <td>
                        <select onchange="editUserPrivelegies({{ $user->id }},this)" id='access' name='access' class='form-control'>
                            <option {{$user->isAdmin() ? 'selected':''}} value="admin">Администратор</option>
                            <option {{$user->isTeacher() ? 'selected':''}} value="teacher">Преподаватель</option>
                            <option {{$user->isStudent() ? 'selected':''}} value="student">Студент</option>
                            <option {{$user->isDisabled() ? 'selected':''}} value="disabled">Отключён</option>
                        </select>
                    </td>
                    <td>
                        <form method="post"
                              onsubmit="return confirm('Вы действительно хотите удалить этого пользователя?');"
                              {{--action="{{ action('UsersController@ajaxAdminDeleteUser', ['userId' => $user->id]) }}">--}}
                            action="{{ action('UsersController@ajaxAdminDeleteUser', ['userId' => $user->id]) }}">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger">Удалить</button>
                        </form></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if(Request::input('filter_group'))
            {{ $users->appends(['filter_group' => Request::input('filter_group')])->links() }}
        @else
            {{ $users->links() }}
        @endif
    </div>
@endsection