@extends('layouts.app')

@section('content')
    <div class="panel-heading">
        <span class="panel-title">Панель управления</span>
    </div>
    <section class="panel-body">
        <h3>Группы</h3>
        <p>Вы можете <span><a href="#" onclick="showGroupForm()">создать группу</a></span></p>
        <table class="table">
            <thead>
            <tr>
                <th>Группа</th>
                <th>Специальность</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @if(count($groups)==0)
                <tr>
                    <td colspan="3">Нет данных</td>
                </tr>
            @endif
            @foreach($groups as $group)
                <tr class="group-{{ $group->id }}">
                    <td class="name">{{ $group->name }}</td>
                    <td class="specialty">{{ $group->specialty->name}}</td>
                    <td>
                        <span><a href="#" onclick="showEditGroupForm({{ $group->id }})">Редакиторовать</a></span>
                        <span> | </span>
                        <span><a href="#" onclick="deleteGroup({{ $group->id }})">Удалить</a></span>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $groups->links() }}
    </section>
    <div class="modal fade" id="group-form-modal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Группа</h4>
                </div>
                <div class="modal-body">
                    <form id="group-form">
                        <input type="hidden" id="group_id" name="group_id">
                        <div class="form-group">
                            <label for="specialty_id">Специальность:</label>
                            <select name="specialty_id" class="form-control" id="specialty_id" style="width: 100%;">
                                <option></option>
                            </select>
                            <span id='specialty_id-error' class="label label-danger" style="display: none"></span>
                        </div>
                        <div class="form-group">
                            <label for="name">Название группы:</label>
                            <input type="text" class="form-control" id="name" name="name" required placeholder="Введите название группы">
                            <span id='name-error' class="label label-danger" style="display: none"></span>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="saveGroup()">Принять</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                </div>
            </div>
        </div>
    </div>
@endsection