@extends('layouts.app')

@section('content')
    <div class="panel-heading">
        <span class="panel-title">Панель управления</span>
    </div>
    <section class="panel-body">
        <h3>Специальности</h3>
        <p>Вы можете <span><a href="#" onclick="showSpecForm()">создать специальность</a></span></p>
        <table class="table">
            <thead>
            <tr>
                <th>Специальность</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @if(count($specs)==0)
                <tr>
                    <td colspan="2">Нет данных</td>
                </tr>
            @endif
            @foreach($specs as $spec)
                <tr class="spec-{{ $spec->id }}">
                    <td class="name">{{ $spec->name }}</td>
                    <td>
                        <span><a href="#" onclick="showEditSpecForm({{ $spec->id }})">Редакиторовать</a></span>
                        <span> | </span>
                        <span><a href="#" onclick="deleteSpec({{ $spec->id }})">Удалить</a></span>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $specs->links() }}
    </section>
    <div class="modal fade" id="spec-form-modal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Специальность</h4>
                </div>
                <div class="modal-body">
                    <form id="spec-form">
                        <input type="hidden" id="spec_id" name="spec_id">
                        <div class="form-group">
                            <label for="name">Название специальности:</label>
                            <input type="text" class="form-control" id="name" name="name" required placeholder="Введите название специальности">
                            <span id='name-error' class="label label-danger" style="display: none"></span>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="saveSpec()">Принять</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                </div>
            </div>
        </div>
    </div>
@endsection