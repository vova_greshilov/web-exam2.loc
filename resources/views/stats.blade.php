@extends('layouts.app')

@section('content')

    <section class="panel-body">
        <h3>Результаты пройденных тестирований</h3>
        <p>Отфильтровать список по параметрам</p>
        <form class='form' action="{{ route('getUserStats') }}" method="get">
            <div class="form-group">
                <label for="filter_specialty">Специальность</label>
                <select id="filter_specialty" name="filter_specialty" class="form-control filter_specialty" onchange="loadDisciplines(this)" style="width: 100%;">
                    <option value="all">Все</option>
                    @foreach(\App\Specialty::all() as $spec)
                        <option {{Request::input('filter_specialty')==$spec->id ? 'selected' : ''}} value="{{ $spec->id }}">{{$spec->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="filter_discipline">Дисциплина</label>
                <select id="filter_discipline" name="filter_discipline" class="form-control filter_discipline" onchange="loadSubjects(this)" style="width: 100%;">
                    <option value="all">Все</option>
                    @foreach(\App\Discipline::all() as $discipline)
                        <option {{Request::input('filter_discipline')==$discipline->id ? 'selected' : ''}} value="{{ $discipline->id }}">{{$discipline->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="filter_subject">Тема</label>
                <select id="filter_subject" name="filter_subject" class="form-control filter_subject" style="width: 100%;">
                    <option value="all">Все</option>
                    @foreach(\App\Subject::all() as $subject)
                        <option {{Request::input('filter_subject')==$subject->id ? 'selected' : ''}} value="{{ $subject->id }}">{{$subject->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <button class="btn btn-default">Обновить список</button>
            </div>
        </form>
        <table class="table">
            <thead>
            <tr>
                <th>Дата прохождения</th>
                <th>Тема, дисциплина, специальность</th>
                <th>Студент</th>
                <th>Набранный балл</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @if(count($stats)==0)
                <tr>
                    <td colspan="2">Нет данных</td>
                </tr>
            @endif
            @foreach($stats as $stat)
                <tr class="stat-{{ $stat->id }}">
                    <td><a href="{{ route('getUserStat', [ 'stat_id' => $stat->id ]) }}">{{ $stat->created_at }}</a></td>
                    <td>
                        <a href="{{ route('getUserStat', [ 'stat_id' => $stat->id ]) }}">
                            {{ $stat->exam->subject->name.' - '
                            .$stat->exam->subject->discipline->name
                            .' ('.$stat->exam->subject->discipline->specialty->name.')' }}
                        </a>
                    </td>
                    <td>
                        <a href="{{ route('getUserStat', [ 'stat_id' => $stat->id ]) }}">
                            {{ $stat->user->name}}
                        </a>
                    </td>
                    <td>
                        <a href="{{ route('getUserStat', [ 'stat_id' => $stat->id ]) }}">
                            <strong>{{ $stat->pointsPercent }}%</strong><br/>
                            <small>{{ $stat->earnedPoints }}/{{ $stat->totalPoints }}</small>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if(Request::input('filter_specialty'))
            {{ $stats->appends(['filter_specialty' => Request::input('filter_specialty')])->links() }}
        @endif
        @if(Request::input('filter_specialty'))
            {{ $stats->appends(['filter_specialty' => Request::input('filter_specialty')])->links() }}
        @endif
        @if(Request::input('filter_specialty'))
            {{ $stats->appends(['filter_specialty' => Request::input('filter_specialty')])->links() }}
        @endif
        @if(Request::input('filter_specialty'))
            {{ $stats->appends(['filter_specialty' => Request::input('filter_specialty')])->links() }}
        @endif
        {{ $stats->links() }}
    </section>
@endsection