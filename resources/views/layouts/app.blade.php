<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Электронное тестирование') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">

    <style type='text/css'>
        .scrollable {
            height: 300px;
            overflow: auto;
        }
    </style>
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
<div id="app">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Открыть меню</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Электронное тестирование') }}
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav nav-pills">
                @if (Auth::check())
                    <!--Пункты меню для всех -->
                        <li class="nav-item">
                            <a href="{{ route('profile') }}">Личный кабинет</a>
                        </li>
                        <!--Пункты меню для студентов -->
                        @if( Auth::user()->isStudent() )
                            <li class="nav-item">
                                <a href="{{ route('getStudentExams') }}">Тесты</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('getUserStats') }}">Статистика</a>
                            </li>
                        @endif
                    <!--Пункты меню для учитилей -->
                        @if( Auth::user()->isTeacher() || Auth::user()->isAdmin() )
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Управление тестами <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ route('getTeacherExams') }}">Тесты</a></li>
                                    <li><a href="{{ route('getTeacherQuestions') }}">Вопросы</a></li>
                                    <li><a href="{{ route('getTeacherSubjects') }}">Темы</a></li>
                                    <li><a href="{{ route('getUserStats') }}">Просмотр результатов</a></li>
                                </ul>
                            </li>
                            <li class="nav-item">

                            </li>
                            <li class="nav-item">

                            </li>
                            <li class="nav-item">

                            </li>
                            <li class="nav-item">

                            </li>
                        @endif
                    <!--Пункты меню для администраторов -->
                        @if( Auth::user()->isAdmin() )
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Управление системой <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a class="dropdown-item" href="{{ route('getAdminSpecialties') }}">Специальности</a></li>
                                    <li><a class="dropdown-item" href="{{ route('getAdminGroups') }}">Группы</a></li>
                                    <li><a class="dropdown-item" href="{{ route('getAdminDisciplines') }}">Дисциплины</a></li>
                                    <li><a class="dropdown-item" href="{{ route('getAdminUsers') }}">Пользователи</a></li>
                                </ul>
                            </li>
                        @endif
                    @endif
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ route('login') }}">Вход</a></li>
                        <li><a href="{{ route('register') }}">Регистрация</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Выйти
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/select2.min.js') }}"></script>
<script src="{{ asset('js/jquery-ui.js') }}"></script>
<script src="{{ asset('js/register.js') }}"></script>
<script src="{{ asset('js/groups.js') }}"></script>
<script src="{{ asset('js/specialties.js') }}"></script>
<script src="{{ asset('js/disciplines.js') }}"></script>
<script src="{{ asset('js/users.js') }}"></script>
<script src="{{ asset('js/questions.js') }}"></script>
<script src="{{ asset('js/subjects.js') }}"></script>
<script src="{{ asset('js/exams.js') }}"></script>
<script src="{{ asset('js/studentExams.js') }}"></script>
<script src="{{ asset('js/stats.js') }}"></script>
</body>
</html>
