@extends('layouts.app')

@section('content')
    <div class="panel-heading">
        <span class="panel-title">Статистика</span>
    </div>
    <section class="panel-body">
        <h3>Результаты тестирования студента:</h3>
        <h4>{{ $stat->user->name }}</h4>
        <h5>От {{ $stat->created_at}}</h5>
        @if($stat->pointsPercent <= 50)
            <h1 class="text-center bg-danger">ТЕСТ НЕ ПРОЙДЕН</h1>
        @endif
        <p><strong>Специальность: </strong>{{ $stat->exam->subject->discipline->specialty->name }}</p>
        <p><strong>Дисциплина: </strong>{{ $stat->exam->subject->discipline->name }}</p>
        <p><strong>Тема: </strong>{{ $stat->exam->subject->name }}</p>
        <p><strong>Набрано баллов: </strong>{{ $stat->earnedPoints }}</p>
        <p><strong>Из возможных: </strong>{{ $stat->totalPoints }}</p>
        <p><strong>Тест пройден на {{ $stat->pointsPercent }}%</strong></p>
        @foreach($stat->examQuestions as $key => $question)
            <div class="panel panel-default">
                <div class="panel-default">
                    <p>Вопрос № {{ $key + 1 }} <code class="label label-success" id="type">
                            @if($question->type=='selectable')
                                Выбор правильного ответа
                            @elseif($question->type=='open')
                                Ввод правильного ответа
                            @elseif($question->type=='mapping')
                                Сопоставление вариантов
                            @elseif($question->type=='sequence')
                                Выстраивание последовательности
                            @endif
                        </code></p>
                    <textarea id='text' class="{{ $question->pivot->is_passed ? "bg-success" : "bg-danger"}}" readonly style="resize: none; width: 100%; height: 100px;">{{ $question->text }}</textarea>
                    @if(isset($question->img))
                        <img id="image"
                             style='margin-left:auto; margin-right:auto;'
                             class='img-responsive img-rounded'
                             src="{{ $question->img }}"
                             alt="У вопроса нет изображения">
                    @endif
                    <p>Баллов набрано:
                        @if($question->type=='selectable')
                            {{ 1 * $question->pivot->is_passed }}
                        @elseif($question->type=='open')
                            {{ 2 * $question->pivot->is_passed }}
                        @elseif($question->type=='mapping')
                            {{ 3 * $question->pivot->is_passed }}
                        @elseif($question->type=='sequence')
                            {{ 4 * $question->pivot->is_passed }}
                        @endif
                    </p>
                </div>
            </div>
        @endforeach
    </section>
@endsection