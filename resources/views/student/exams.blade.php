@extends('layouts.app')

@section('content')
    <div class="panel-heading">Список доступных тестирований</div>
    <div class="panel-body">
        <table class="table table-condensed">
            <thead>
            <tr>
                <th>Создано</th>
                <th>Тема</th>
                <th>Экзаменатор</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @if(count($exams)==0)
                <tr>
                    <td colspan="4">Ничего не найдено</td>
                </tr>
            @endif
            @foreach ($exams as $exam)
                <tr>
                    <td>{{ $exam->created_at }}</td>
                    <td>{{ $exam->subject->name }}<br/><strong>(вопросов в тесте - {{ $exam->questions_count }})</strong></td>
                    <td>{{ $exam->creator->name }}</td>
                    <td>
                        @if($exam->passedBy(Auth::user()))
                            <p>Вы уже прошли этот тест.</p>
                        @else
                            <a href="{{ route('getStudentExamForm', ['id' => $exam->id, 'training' => true]) }}" class='btn btn-xs btn-primary btn-block'>Тренировка</a>
                            <a href="{{ route('getStudentExamForm', ['id' => $exam->id]) }}" class='btn btn-xs btn-success  btn-block confirmation'>Начать тестирование</a>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection