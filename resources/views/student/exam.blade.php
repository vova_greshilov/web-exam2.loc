@extends('layouts.app')

@section('content')
    <div class="panel-heading">
        <p>Дисциплина: {{ $stat->exam->subject->discipline->name }}</p>
        <p>Тестирование по теме: {{ $stat->exam->subject->name }}</p>
    </div>
    <div class="panel-body">
        <h1>Вопрос <span class="current-question">1</span>/{{ $stat->exam->questions_count }}</h1>
        <form id="questionForm">
            <input type="hidden" id='stat-id' value="{{ $stat->id }}">
            <input type="hidden" id='question-id' value="">
            <img id="img" style='margin-left:auto; margin-right:auto; display: none;' class='img-responsive img-rounded' src="" alt="Изображение недоступно">
            </br>
            <p><code class="label label-success" style="display: none" id="type"></code></p>
            </br>
            <textarea id='text' readonly style="resize: none; width: 100%; height: 300px;">Загрузка вопрса...</textarea>
            <div id="answers-div">
            </div>
            <div id="answer-div">
                <input type="text" id="answer" name="answer">
            </div>
            <hr>
            <div id="buttons-div" style="display: none;">
                <div class='form-group'>
                    <button id='btn-confirm' class="btn btn-primary">Подтвердить ответ</button>
                </div>
            </div>
        </form>
    </div>
@endsection