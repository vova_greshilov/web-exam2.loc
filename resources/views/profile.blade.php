@extends('layouts.app')

@section('content')
    <div class="panel-heading">Личный кабинет</div>
    <div class="panel-body">
        <!--На данной странице в зависимотси от типа пользователя будут секции контента</br>-->
        @if( Auth::user()->isStudent() )
            @include('profile.student')
        @elseif( Auth::user()->isTeacher() )
            @include('profile.teacher')
        @elseif( Auth::user()->isAdmin() )
            @include('profile.admin')
        @else
        <p>Пользователь не активирован администратором</p>
        @endif
    </div>
@endsection
