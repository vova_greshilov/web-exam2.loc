<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name' => env('ADMIN_NAME','Грешилов Владимир Алексеевич'),
            'email' => env('ADMIN_EMAIL','vgreshilov@mail.ru'),
            'password' => bcrypt(env('ADMIN_PASSWORD','qqqqqq')),
            'group_id' => null,
            'access' => 'admin',
        ]);
    }
}
