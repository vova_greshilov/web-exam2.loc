<?php

use Illuminate\Database\Seeder;

class TestUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name' => 'Иванов И.И.',
            'email' => 'admin@test.loc',
            'password' => bcrypt('ketvy8'),
            'group_id' => \App\Group::where('name', 'БП-1')->first()->id,
            'access' => 'admin',
        ]);

        \App\User::create([
            'name' => 'Петрова В.В.',
            'email' => 'teacher@test.loc',
            'password' => bcrypt('ws55fc'),
            'group_id' => \App\Group::where('name', 'БП-1')->first()->id,
            'access' => 'teacher',
        ]);

        \App\User::create([
            'name' => 'Егоров Е.Е,',
            'email' => 'student@test.loc',
            'password' => bcrypt('f74mga'),
            'group_id' => \App\Group::where('name', 'БП-1')->first()->id,
            'access' => 'student',
        ]);
    }
}
