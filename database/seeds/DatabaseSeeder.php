<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(AdminSeeder::class);
        $this->call(SpecialtiesAndGroupsSeeder::class);
        $this->call(TestUser::class);
        $this->call(InformationSecutiryTest::class);
        $this->call(DatabaseTheoryTest::class);
    }
}
