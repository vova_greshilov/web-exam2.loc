<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDbStructure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('specialties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',100);
        });

        Schema::create('groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',100);
            $table->integer('specialty_id')->unsigned();
            $table->foreign('specialty_id')->references('id')->on('specialties')->onDelete('cascade');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->integer('group_id')->unsigned()->nullable();
            $table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade');
            $table->enum('access', ['admin', 'teacher','student','disabled'])->default('disabled');
        });

        Schema::create('disciplines', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',100);
            $table->integer('specialty_id')->unsigned();
            $table->foreign('specialty_id')->references('id')->on('specialties')->onDelete('cascade');
        });

        Schema::create('subjects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',100);
            $table->integer('discipline_id')->unsigned();
            $table->foreign('discipline_id')->references('id')->on('disciplines')->onDelete('cascade');
        });

        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', ['open', 'selectable', 'sequence', 'mapping'])->default('open');
            $table->string('text',2512);
            $table->string('img')->nullable();
            $table->integer('subject_id')->unsigned();
            $table->foreign('subject_id')->references('id')->on('subjects')->onDelete('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::create('answers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('text',1024);
            $table->boolean('is_right')->default(false);
            $table->integer('question_id')->unsigned();
            $table->foreign('question_id')->references('id')->on('questions')->onDelete('cascade');
        });

        Schema::create('exams', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('close_count')->unsigned()->default(2);
            $table->integer('open_count')->unsigned()->default(2);
            $table->integer('sequence_count')->unsigned()->default(2);
            $table->integer('mapping_count')->unsigned()->default(2);
            $table->boolean('is_active')->default(true);
            $table->integer('subject_id')->unsigned();
            $table->foreign('subject_id')->references('id')->on('subjects')->onDelete('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('stats', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('is_completed')->default(false);
            $table->boolean('is_training')->default(false);
            $table->integer('exam_id')->unsigned();
            $table->foreign('exam_id')->references('id')->on('exams')->onDelete('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('exam_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('is_passed')->default(false);
            $table->integer('question_id')->unsigned();
            $table->foreign('question_id')->references('id')->on('questions')->onDelete('cascade');
            $table->integer('stat_id')->unsigned();
            $table->foreign('stat_id')->references('id')->on('stats')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('exam_questions');
        Schema::dropIfExists('stats');
        Schema::dropIfExists('exams');
        Schema::dropIfExists('answers');
        Schema::dropIfExists('questions');
        Schema::dropIfExists('subjects');
        Schema::dropIfExists('disciplines');
        Schema::table('users', function($table) {
            $table->dropColumn('access');
            $table->dropForeign(['group_id']);
            $table->dropColumn('group_id');
        });
        Schema::dropIfExists('groups');
        Schema::dropIfExists('specialties');
    }
}
