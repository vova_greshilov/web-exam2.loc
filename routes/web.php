<?php

Route::get('/', function () {
    if (Auth::guest()) {
        return view('welcome');
    }
    return redirect()->route('profile');
});

Auth::routes();

Route::group(['prefix' => 'api'], function () {
    Route::get('groups', 'GroupsController@ajaxApiGetGroups');
    Route::get('specialties', 'SpecialtiesController@ajaxApiGetSpecialties');
    Route::get('disciplines', 'DisciplinesController@ajaxApiGetDisciplines');
    Route::get('subjects', 'SubjectsController@ajaxApiGetSubjects');

});

Route::group(['middleware' => 'auth'], function () {
    Route::get('/profile', 'PagesController@profile')->name('profile');



    Route::group(['prefix' => 'admin','middleware' => 'admin'], function () {
        Route::get('/specs','SpecialtiesController@getAdminSpecialties')->name('getAdminSpecialties');
        Route::get('/specs/delete', 'SpecialtiesController@ajaxAdminDeleteSpecialty')->name('ajaxAdminDeleteSpecialty');
        Route::post('/specs/create', 'SpecialtiesController@ajaxAdminCreateSpecialty')->name('ajaxAdminCreateSpecialty');
        Route::post('/specs/edit', 'SpecialtiesController@ajaxAdminEditSpecialty')->name('ajaxAdminEditSpecialty');
        Route::get('/spec', 'SpecialtiesController@ajaxAdminSpecialty')->name('ajaxAdminSpecialty');

        Route::get('/groups', 'GroupsController@getAdminGroups')->name('getAdminGroups');
        Route::get('/groups/delete', 'GroupsController@ajaxAdminDeleteGroup')->name('ajaxAdminDeleteGroup');
        Route::post('/groups/create', 'GroupsController@ajaxAdminCreateGroup')->name('ajaxAdminCreateGroup');
        Route::post('/groups/edit', 'GroupsController@ajaxAdminEditGroup')->name('ajaxAdminEditGroup');
        Route::get('/group', 'GroupsController@ajaxAdminGroup')->name('ajaxAdminGroup');

        Route::get('/disciplines', 'DisciplinesController@getAdminDisciplines')->name('getAdminDisciplines');
        Route::get('/disciplines/delete', 'DisciplinesController@ajaxAdminDeleteDiscipline')->name('ajaxAdminDeleteDiscipline');
        Route::post('/disciplines/create', 'DisciplinesController@ajaxAdminCreateDiscipline')->name('ajaxAdminCreateDiscipline');
        Route::post('/disciplines/edit', 'DisciplinesController@ajaxAdminEditDiscipline')->name('ajaxAdminEditDiscipline');
        Route::get('/discipline', 'DisciplinesController@ajaxAdminDiscipline')->name('ajaxAdminDiscipline');

        Route::get('/users', 'UsersController@getAdminUsers')->name('getAdminUsers');
        Route::post('/users/edit', 'UsersController@ajaxAdminEditUser')->name('ajaxAdminEditUser');
        Route::post('/users/delete/{userId}', 'UsersController@ajaxAdminDeleteUser');
    });

    //Routes for teahcer
    Route::group(['prefix' => 'teacher','middleware' => 'teacher'], function () {
        Route::get('/questions','QuestionsController@getTeacherQuestions')->name('getTeacherQuestions');
        Route::get('/questions/delete', 'QuestionsController@ajaxTeacherDeleteQuestion')->name('ajaxTeacherDeleteQuestion');
        Route::get('/question','QuestionsController@ajaxTeacherQuestion')->name('ajaxTeacherQuestion');
        Route::post('/questions/create','QuestionsController@ajaxTeacherCreateQuestion')->name('ajaxTeacherCreateQuestion');
        Route::post('/questions/edit', 'QuestionsController@ajaxTeacherEditQuestion')->name('ajaxTeacherEditQuestion');
        Route::get('/questions/answer/detach', 'QuestionsController@ajaxTeacherDeleteAnswer')->name('ajaxTeacherDetachAnswer');
        Route::post('/questions/import', 'QuestionsController@importQuestions');

        Route::get('/subjects','SubjectsController@getTeacherSubjects')->name('getTeacherSubjects');
        Route::get('/subjects/delete', 'SubjectsController@ajaxTeacherDeleteSubject')->name('ajaxTeacherDeleteSubject');
        Route::get('/subject','SubjectsController@ajaxTeacherSubject')->name('ajaxTeacherSubject');
        Route::post('/subjects/create','SubjectsController@ajaxTeacherCreateSubject')->name('ajaxTeacherCreateSubject');
        Route::post('/subjects/edit', 'SubjectsController@ajaxTeacherEditSubject')->name('ajaxTeacherEditSubject');

        Route::get('/exam','ExamController@getTeacherExams')->name('getTeacherExams');
        Route::get('/exam/delete','ExamController@ajaxTeacherDeleteExam')->name('ajaxTeacherDeleteExam');
        Route::get('/exam/edit','ExamController@ajaxTeacherEditExam')->name('ajaxTeacherEditExam');
        Route::post('/exam/create','ExamController@ajaxTeacherCreateExam')->name('ajaxTeacherCreateExam');


    });

    Route::group(['prefix' => 'student', 'middleware' => 'student'],function () {
        Route::get('/exam','ExamController@getStudentExams')->name('getStudentExams');
        Route::get('/exam/question', 'ExamController@ajaxGetExamQuestion')->name('ajaxGetExamQuestion');
        Route::get('/exam/{exam_id}','ExamController@getStudentExamForm')->name('getStudentExamForm');
        Route::post('/exam/question/confirm', 'ExamController@ajaxStudentConfirmAnswer')->name('ajaxStudentConfirmAnswer');
    });

    Route::get('/stat', 'StatController@getUserStats')->name('getUserStats');
    Route::get('/stat/{stat_id}', 'StatController@getUserStat')->name('getUserStat');
});


/*Route::group(['prefix' => 'api'], function () {
    //Api routes for ajax requests
    Route::post('/qset', 'ExamController@ajaxQuestionSetBySpecialty')->name('ajaxGetExamQuestion');
    Route::post('/confirm-answer', 'StudentExamController@ajaxConfirmAnswer')->name('ajaxConfirmAnswer');
    Route::post('/exam-question', 'StudentExamController@ajaxGetExamQuestion')->name('ajaxGetExamQuestion');
});


//доступ будет только для зарегестрированных пользователей
Route::group(['middleware' => 'auth'], function () {

    Route::get('/profile', 'ProfileController@profile')->name('profile');

    Route::group(['prefix' => 'student', 'middleware' => 'student'],function () {
        //Exam
        Route::get('/exam','StudentExamController@getStudentExamList')->name('getStudentExamList');
        Route::get('/exam/{id}','StudentExamController@getStudentExamForm')->name('getStudentExamForm');
        //Stats
        Route::get('/stat','StatController@getStudentStatList')->name('getStudentStatList');
        Route::get('/stat/{stat_id}','StatController@getStudentStatView')->name('getStudentStatView');

    });

    //Routes for teahcer
    Route::group(['prefix' => 'teacher','middleware' => 'teacher'], function () {
        //Question Set
        Route::get('/qset','QuestionSetController@getQuestionSetList')->name('getQuestionSetList');
        Route::get('/qset/form/{id?}','QuestionSetController@getQuestionSetForm')->name('getQuestionSetForm');
        Route::post('/qset/add','QuestionSetController@addQuestionSet')->name('addQuestionSet');
        Route::post('/qset/edit/{id}','QuestionSetController@editQuestionSet')->name('editQuestionSet');
        Route::get('/qset/{id}','QuestionSetController@getQuestionSetView')->name('getQuestionSetView');
        Route::post('/qset/delete/{id}', 'QuestionSetController@deleteQuestionSet')->name('deleteQuestionSet');
        //Question
        Route::get('/qset/{qset}/question/form/{id?}', 'QuestionController@getQuestionForm')->name('getQuestionForm');
        Route::post('/question/add', 'QuestionController@addQuestion')->name('addQuestion');
        Route::post('/question/edit/{id}', 'QuestionController@editQuestion')->name('editQuestion');
        Route::get('/question/{id}', 'QuestionController@getQuestionView')->name('getQuestionView');
        Route::post('/question/delete/{id}', 'QuestionController@deleteQuestion')->name('deleteQuestion');
        //Answer
        Route::get('/question/{qid}/answ/form/{id?}','AnswerContoller@getAnswerForm')->name('getAnswerForm');
        Route::post('/answ/add','AnswerContoller@addAnswer')->name('addAnswer');
        Route::post('/answ/edit/{id}','AnswerContoller@editAnswer')->name('editAnswer');
        Route::post('/answ/delete/{id}','AnswerContoller@deleteAnswer')->name('deleteAnswer');
        //Exam
        Route::get('/exam','ExamController@getExamList')->name('getExamList');
        Route::get('/exam/form/{id?}','ExamController@getExamForm')->name('getExamForm');
        Route::post('/exam/add','ExamController@addExam')->name('addExam');
        Route::post('/exam/edit/{id}','ExamController@editExam')->name('editExam');
        //Stats
        Route::get('/stat','StatController@getTeacherStatList')->name('getTeacherStatList');
//        Route::get('/stat/{id}','StatController@getStudentStatView')->name('getStudentStatView');
    });

    //Routes for admin
    Route::group(['prefix' => 'admin','middleware' => 'admin'], function () {
        //Group
        Route::get('/group/form','GroupController@getAddGroup')->name('getAddGroup');

        Route::post('/group/add','GroupController@addGroup')->name('addGroup');

        //Specialty
        Route::get('/specialty/form', 'SpecialtyController@getAddSpecialty')->name('getAddSpecialty');

        Route::post('/specialty/add', 'SpecialtyController@addSpecialty')->name('addSpecialty');

        //User
        Route::get('/users', 'UserController@getUsersList')->name('getUsersList');

        Route::post('/users', 'UserController@setUserPrivileges')->name('setUserPrivileges');
    });
});*/